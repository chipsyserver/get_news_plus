<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', 'Admin_Pannel@index');


//Route::get('/category', 'Admin_Pannel@category');
Route::get('/category/{name}/{id}/{title}','Admin_Pannel@category_function');

	Route::get('/single_page/{name}/{id}/{title}','Admin_Pannel@single_function'); 
	Route::get('/single_page_city/{id}/{name}','Admin_Pannel@city_function'); 
Route::get('/complient', 'Admin_Pannel@complient');
Route::get('/job', 'Admin_Pannel@job');
Route::get('/contact', 'Admin_Pannel@contact');
Route::post('/job_insert', 'Job_insert@job_insert');
Route::post('/job_insert1', 'Job_insert@job_insert1');
Route::get('/find/{id}', 'Admin_Pannel@find_fun');
Route::get('/getfind/{id}', 'Admin_Pannel@get_fun');
Route::get('/getjob', 'Admin_Pannel@getjob');
Route::get('/mycity', 'Admin_Pannel@getmycity');
Route::get('/getmycity', 'Admin_Pannel@get_city');
Route::get('/getmycity/{id}/{shop}', 'Admin_Pannel@get_mycity');
// Route::get('/date/{id}', 'Date@get_date');
  Route::post('/contact', 'complient\contact@contact_func');

Route::group(['namespace' => 'complient'], function () {
	  Route::post('complient/insert', 'Insert@insert_func');
	
	


	   
});



// Route::group(['middleware' => 'web'], function () {
//   Route::get('contact', 'ContactController@getContact');
//   Route::post('contact', 'ContactController@postContact');
// });


