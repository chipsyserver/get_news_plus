<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// die(json_encode("anus"));

Route::get('../img{category}/{year}/{month}/{name}.{ext}',function ($category, $year,$month,$name,$ext) {
header('Content-type: image/jpeg');
die(file_get_contents("/img$category/$year/$month/$name"."." .$ext));
});


Route::get('/{category}/{year}/{month}/{name}.{ext}',function ($category, $year,$month,$name,$ext) {
header('Content-type: image/jpeg');
die(file_get_contents("/$category/$year/$month/$name"."." .$ext));
});

Route::get('/', 'Login@index');
Route::get('/home', 'Admin_Pannel@index');
Route::get('/logout','Admin_Pannel@getLogout');

Route::group(['namespace' => 'Auth'], function () {
	  Route::post('auth/postlogin', 'AuthController@postLogin');

// Route::post('superadmin/setting',   'AuthController@update_account');

	   
});

Route::group(['middleware' =>'guest'],  function () {
// Route::group(['middleware' => 'disablehistry'], function () {

Route::group(['prefix' => 'news'], function () {
 Route::get('/create', 'Admin_Pannel@news_create');
  Route::get('/display', 'Admin_Pannel@news_display');
 Route::get('/getdisplay', 'Admin_Pannel@news_fetch');
 Route::get('/edit{id}','news\edit@editsingle_func'); 
  Route::get('/international', 'Admin_Pannel@news_international');
  Route::get('/national', 'Admin_Pannel@news_national');
  Route::get('/state', 'Admin_Pannel@news_state');
  Route::get('/local', 'Admin_Pannel@news_local');
  Route::get('/job', 'Admin_Pannel@news_job');
  Route::get('/news_displayjob', 'Admin_Pannel@news_displayjob');
 Route::get('/getdisplayjob', 'Admin_Pannel@news_fetch1');
Route::get('/edit_job/{id}','news\edit@editjob_func');
Route::get('/complient', 'Admin_Pannel@news_complient');
Route::get('/getcomplient', 'Admin_Pannel@complient');
Route::get('/profile', 'Admin_Pannel@news_profile');
Route::get('/getprofile', 'Admin_Pannel@profile');

Route::get('/setting',   'Admin_Pannel@settings');
Route::get('/what-is-in-my-city', 'Admin_Pannel@mycity');
Route::get('/edit_mycity/{id}','news\edit@editmycity_func');
Route::get('/getdisplaymycity', 'Admin_Pannel@mycity_fetch');
 Route::post('/mycity', 'Admin_Pannel@news_mycity');
 Route::get('/addcity', 'Admin_Pannel@news_city');
 Route::get('/displaycity', 'Admin_Pannel@displaycity');
Route::post('/add_city', 'Admin_Pannel@add_city');
Route::get('/shop','Admin_Pannel@shop_func');
Route::get('/displayshop', 'Admin_Pannel@displayshop_func');
Route::post('/add_shop', 'Admin_Pannel@add_shop');
Route::group(['namespace' => 'news'], function () {
	
     Route::delete('/delete/{id}','delete@delete_func');
     Route::delete('/deletejob/{id}','delete@deletejob_func');
     // Route::post('/edit{id}','edit@editsingle_func');editjob_func
	 Route::get('/single{id}','edit@edit_func');


	 Route::post('/store', 'FileController@news_store');
	
	  Route::post('/news_job', 'FileController@news_job');
	 Route::post('/update/{id}','update@update_func');
	  Route::post('/updatejob/{id}','update@updatejob_func');
	  Route::post('/updatecity/{id}','update@updatecity_func');
	   Route::post('/update_city/{id}','update@update_city');
	   Route::post('/update_shop/{id}','update@update_shop');
	  
Route::post('update_account',   'update_account@update_account');
	 Route::delete('/deletemycity/{id}','delete@deletemycity_func');
	  Route::delete('/city/{id}','delete@deletecity_func');
	 Route::delete('/shop/{id}','delete@deleteshop_func');

	 // Route::post('/edit{id}','edit@editsingle_func');

	// Route::post('/insert', 'Insert@insert_func');
	

	});
 //Route::post('/insert', 'Insert@insert_func');





 
 // });	
});
});


