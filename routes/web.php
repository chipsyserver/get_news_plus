<?php

Route::group(['namespace' => 'Web\Backend','prefix' => 'superadmin'], function () {
    require(__DIR__ . '/Web/web_backend_controls.php');
});

Route::group(['namespace' => 'Web\Frontend'], function () {
    require(__DIR__ . '/Web/web_frontend_controls.php');
});

?>
