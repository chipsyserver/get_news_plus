<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
	
    protected $table = 'admin';

    protected $fillable = [ 'id','user_name', 'password','remember_token','last_login' ];

    public $timestamps = true;
}
