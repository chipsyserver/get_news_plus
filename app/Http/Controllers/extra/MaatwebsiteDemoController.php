<?php
namespace App\Http\Controllers;
use App\Item;
use Input;
use Illuminate\Http\Request;
use DB;
use Validator;
use Excel;
class MaatwebsiteDemoController extends Controller
{
	public function importExport()
	{
		return view('importExport');
	}
	// public function downloadExcel($type)
	// {
	// 	$data = Item::get()->toArray();
	// 	return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
	// 		$excel->sheet('mySheet', function($sheet) use ($data)
	//         {
	// 			$sheet->fromArray($data);
	//         });
	// 	})->download($type);
	// }
	public function importExcel(Request $request)
	{
		// $inputs=$request->all();
		// die(json_encode($inputs['import_file']->getRealPath()));
		// 	if(Input::hasFile('import_file')){
		// 	$path = $inputs['import_file']->getRealPath();
		// 	$data = Excel::load($path, function($reader) {
				
		// 	})->get();
		$count = DB::table('count')->get();
 $rules = array(
      					 	 'import_file' => 'required',
      					 	
   							 );

    
      $validator = Validator::make($request->all(), $rules); 
		$data = $request->all();
	
	    if ($validator->fails()) {
			return response()->json(array(
					'success' => false,
					'message' => $validator->getMessageBag()->toArray()
					));
	   }




$allowed =  array('xlsx', 'ods');
$filename = $_FILES['import_file']['name'];
$ext = pathinfo($filename, PATHINFO_EXTENSION);
if(!in_array($ext,$allowed) ) {
   return response()->json(array(
		            	'success' => false,
						'message' => "Extension error"
						));
				}

	   			$file = $request->file('import_file');
			//die(json_encode($file->getClientOriginalExtension()));
			
			// die(json_encode($file['import_file']));
        if($file){
            $path = $file->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();



         


			if(!empty($data) && $data->count()){
				
		         $stored_msg=$data[0]['message'];
		         $tot=$count[0]->presentcount+$data->count();
		         if ($tot>=1000) {
					return response()->json(array(
		            	'success' => false,
						'message' => "U cross the limit"
						));
				}
				 

				foreach ($data as $key =>  $value) {
					
				
					$mobile=$value->mobile;
					$message=$value->message;

					$mob="/^[6789][0-9]{9}$/";
			if(!preg_match($mob,$mobile))
			{	  return response()->json(array(
		            	'success' => false,
		            	// 'number'=>$value->mobile,
		            	// 'pos'=>$key,
						'message' => "Invalid  number ".$value->mobile."<br>Line Number :".$key
						));
			}


 

			
		
					
				// 		$rules = array($message => 'max:160',$mobile => 'required');
	   // 				$validator = Validator::make($request->all(), $rules); 
				// 	$data = $request->all();
	
	   //  if ($validator->fails()) {
			 // return response()->json(array(
			 // 		'success' => false,
			 // 		'message' => $validator->getMessageBag()->toArray()
			 // 		));
	   // }  
		
		// if (filter_var($data['email_address'], FILTER_VALIDATE_EMAIL) === false) {
		//      return response()->json(array(
		//             	'success' => false,
		// 				'message' => "Invalid Email address."
		// 				));
		// }
		 //    $mob="/^[6789][0-9]{9}$/";
			// if(!preg_match($mob,$mobile)
			// {	  return response()->json(array(
		 //            	'success' => false,
			// 			'message' => "Invalid Mobile number."
			// 			));
			// }
					
				 if($message==null)
				 {
					
					$message = $stored_msg;
					
			      }
			
					$insert[] = ['mobile' => $mobile,'message' =>$message];

					
		}

				}
				// if(!empty($insert)){
					DB::table('items')->insert($insert);
					return response()->json(array(
	          		'success' => true,
					'message' => "insert success."
					));
				// }

			}

				
		return back();
	
	     }
		
	}
