<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use App\listnews;
use App\listjob;
use Auth;
use DB;
class Admin_Pannel extends Controller {
	 	
	public function __construct()
	{
		$this->middleware('auth');
	}


	
	public function news_international()
	{
		

		return view('Backend.news.news_international' )->with(array('data'=>listnews::listnews()));
     
	}
	public function news_job()
	{
		

		return view('Backend.news.news_jobpost' )->with(array('data'=>listjob::listjob()));
     
	}
	public function news_state()
	{
		
		             


		return view('Backend.news.news_state' )
		->with(array('data'=>listnews::listnews()));
     
	}
	public function news_local()
	{
		

		return view('Backend.news.news_local' )->with(array('data'=>listnews::listnews()));
     
	}
	public function news_national()
	{
		

		return view('Backend.news.news_national' )->with(array('data'=>listnews::listnews()));
     
	}

	public function news_create()
	{
		

		return view('Backend.news.news_create' )->with(array('data'=>listnews::listnews()));
     
	}

		public function news_display()
	{
		return view('Backend.news.news_create' )->with(array('data'=>listnews::listnews()));
	}

	public function news_fetch()
	{
		return listnews::listnews();
	}


// job post display
	public function news_fetch1()
	{
		return listjob::listjob();
	}



	public function news_displayjob()
	{
		return view('Backend.news.news_job' )->with(array('data'=>listjob::listjob()));
	}

//end
	


	
	
	 public function getLogout()
	{	
	  Auth::logout();
	  return redirect('/');
	}

	

}
