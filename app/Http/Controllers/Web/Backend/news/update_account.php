<?php 
namespace App\Http\Controllers\Web\Backend\news;
use App\Http\Controllers\Controller;
use App\Login;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use DB;
use Hash;
use Auth;
class update_account extends Controller {


public function update_account(Request $request) {
		$rules1 = array('uname' => 'required|max:255','old_password'=>'required');
	    $data = $request->all();
		$validator = Validator::make($request->all(), $rules1); 
	    if ($validator->fails()) {
			return response()->json(array(
					'success' => false,
					'message' => $validator->getMessageBag()->toArray()
					));
		    }
		    $user_id=Auth::user()->id;
			$users = DB::table('admin')
		     	->where('id', $user_id)
				 ->select('password')
                ->first();
			     if($users){
				   if(Hash::check($data['old_password'],$users->password))
				   {
					    $updatedDate = date("Y-m-d h:i:s");
					    $userprofile = DB::table('admin')
		     	                     ->where('id', $user_id)
							         ->update(['user_name'=>$data['uname'],'updated_at'=>$updatedDate]);
					       if(isset($data['change_password'])) {
							 $rules2 = array('new_password' => 'required','re_password'=>'required');
							 $validator = Validator::make($request->all(), $rules2); 
							if ($validator->fails()) {
								return response()->json(array(
											'success' => false,
											'message' => $validator->getMessageBag()->toArray()
									));
								}else {
								 if($data['new_password']==$data['re_password']) {
								   $password=Hash::make($data['new_password']);
								   $userprofile = DB::table('admin')
		     	                               ->where('id', $user_id)
							                   ->update(['password'=>$password,'updated_at'=>$updatedDate]);
									
									}else {
											return response()->json(array(
											'success' => false,
											'message' => 'Password doesnt match.'
									));
									}
								}
						}
					    if($userprofile==1) {
					    return response()->json(array(
						'success' => true,
						'message' => "Account updated successfully ."
						));
					  }
				   } 
	         } 
			return response()->json(array(
						'success' => false,
						'message' => "Invalid old password."
		  ));
					 
	}
}