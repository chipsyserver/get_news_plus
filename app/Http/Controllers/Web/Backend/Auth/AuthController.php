<?php 
namespace App\Http\Controllers\Web\Backend\Auth;
use App\Http\Controllers\Controller;
use App\Login;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use DB;
use Hash;
use Auth;
class AuthController extends Controller {

 public function postLogin(Request $request){
 
 	
        $inputs=$request->all();
        $rules = array('user_name' => 'required|max:255','password' => 'required');
	    $validator = Validator::make($request->all(), $rules); 
		$data = $request->all();
	    if ($validator->fails()) {
			return response()->json(array(
					'success' => false,
					'message' => $validator->getMessageBag()->toArray()
					));
		}  
		$admin = DB::table('admin')
		     	->where('user_name', $data['user_name'])
                ->select('id','password')
                ->first();
			   if($admin){ 
				   if(Hash::check($data['password'],$admin->password))
				   {
					    Auth::attempt(array('user_name' => $data['user_name'], 'password' => $data['password']));
	  					Auth::loginUsingId($admin->id);
					    return response()->json(array(
						'success' => true,
						'message' => "Login successfully completed"
						));
				   }
				}
					    return response()->json(array(
						'success' => false,
						'message' => "Invalid username or password"
						));
				
      
 }
 
 }
 
