<?php 
namespace App\Http\Controllers\Web\Backend\news;
use App\Http\Controllers\Controller;
use  DB;
use Validator;
use Image;
use Hash;
use Illuminate\Http\Request;
class Update extends Controller {

    	public function update_func(Request $request,$id) { 
		 $rules = array('title' => 'required',
            'description' => 'required',
            );
        $validator = Validator::make($request->all(), $rules); 
        $data = $request->all();
    
        if ($validator->fails()) {
            return response()->json(array(
                    'success' => false,
                    'message' => $validator->getMessageBag()->toArray()
                    ));
       }    
	    

		$img = $request->file('product_image');
		if($img!=null) {   
        $imgpath= DB::table('img')->where('news_id',$id)->get();
        
      
         $image_path1=public_path("img".$imgpath[0]->category."/".$imgpath[0]->year."/".$imgpath[0]->month."/".$imgpath[0]->image_name."_l.".$imgpath[0]->ext);
         $image_path2=public_path("img".$imgpath[0]->category."/".$imgpath[0]->year."/".$imgpath[0]->month."/".$imgpath[0]->image_name."_s.".$imgpath[0]->ext);
// $image_path="";
//echo $image_path1;

 if (File_exists($image_path1)) {
        //File::delete($image_path);
        unlink($image_path1);
         unlink($image_path2);
    }
        // if(file_exists($path){
        // unlink($path);
        // }

        
        DB::table('img') ->where('news_id',$id) ->delete();
        $img_cat="img"; $i_year=date('Y'); 
        $i_month=date('m'); $image_name=uniqid(); 
        $ext=$img->getClientOriginalExtension(); 
        if (!is_dir("img" . $img_cat)) {
            mkdir("img" . $img_cat);
        }
        
        if (!is_dir("img" . $img_cat . "/" . $i_year)) {
            mkdir( "img" . $img_cat . "/" . $i_year);
        }
        if (!is_dir("img" . $img_cat . "/" . $i_year . "/" . $i_month)) {
            mkdir("img" . $img_cat . "/" . $i_year . "/" . $i_month);
        }



         list($width, $height) = getimagesize($img);

                 $ratio = $width/$height; // width/height

                 if ($ratio > 1) {
                 $width = 150;
                 $height = 150/$ratio;
                 } else {
                 $width = 150*$ratio;
                 $height = 150;
                 }

       Image::make($img)->resize($width, $height)->save('img'.$img_cat.'/'.$i_year.'/'.$i_month.'/'. $image_name.'_s.'.$ext  );


    

                 if ($ratio > 1) {
                 $width = 600;
                 $height = 600/$ratio;
                 } else {
                 $width = 600*$ratio;
                 $height = 600;
                 }

       Image::make($img)->resize($width, $height)->save('img'.$img_cat.'/'.$i_year.'/'.$i_month.'/'. $image_name.'_l.'.$ext  );

       $img=DB::insert('insert into img(news_id,category,year,month,image_name,ext) values(?,?,?,?,?,?)',array($id,$img_cat,$i_year,$i_month,$image_name,$ext));

   }
	   

                 $title= $request->get('title');
                 $state_id= $request->get('state_id');
                        
                $description =$request->get('description');
                $cat =$request->get('cat');

                 $priority= $request->get('priority');
                
                if($priority!=1)
                {
                    $priority=0;
                }
             

if($cat==103)
{
  if ($state_id==0)
  {
     return response()->json(array(
                        'success' => false,
                        'message' => "Select the state "
                        ));

  }
   $res= DB::table('news')
           ->where('news_id', $id)
           ->update(['state_id'=>$state_id,'title' => $title,'description'=>$description,'priority'=>$priority]);


}
else
{
    $res= DB::table('news')
           ->where('news_id', $id)
           ->update(['title' => $title,'description'=>$description,'priority'=>$priority]);
}
	  
		
	   

            
	 //    if($res==1) {
		// $data['news_id']=$id;
  return response()->json(array(
		                'data' =>$data,
						'success' => true,
						'message' => "news updated successfully inserted."
						));
	   // }
	    // else{
	   
	    //  }

		
	}



    public function updatejob_func(Request $request,$id) { 
         
         $rules = array('title' => 'required',
            'description' => 'required',
            );
        $validator = Validator::make($request->all(), $rules); 
        $data = $request->all();
    
        if ($validator->fails()) {
            return response()->json(array(
                    'success' => false,
                    'message' => $validator->getMessageBag()->toArray()
                    ));
       }    
        

      
       

                 $title= $request->get('title');
                 $job_id= $request->get('job_id');
                        
                $description =$request->get('description');
                
             


  if ($job_id==0)
  {
     return response()->json(array(
                        'success' => false,
                        'message' => "Select the state "
                        ));

  }
   $res= DB::table('jobpost')
           ->where('news_id', $id)
           ->update(['job_id'=>$job_id,'title' => $title,'description'=>$description]);




      
        
       

            
     //    if($res==1) {
        // $data['news_id']=$id;
  return response()->json(array(
                        'data' =>$data,
                        'success' => true,
                        'message' => "updated successfully ."
                        ));
       // }
        // else{
       
        //  }

        
    }
	 
}
