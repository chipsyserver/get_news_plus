<?php
namespace App\Http\Controllers;
use App\Item;
use Input;
use Illuminate\Http\Request;
use DB;
use Excel;
class MaatwebsiteDemoController extends Controller
{
	public function importExport()
	{
		return view('importExport');
	}
	public function downloadExcel($type)
	{
		$data = Item::get()->toArray();
		return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download($type);
	}
	
	public function importExcel(Request $request)
	{
		if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
				
			})->get();
			
			
			if(!empty($data) && $data->count()){
		         $stored_msg=$data[0]['message'];
				 
				foreach ($data as  $value) {
					
				
					$mobile=$value->mobile;
					$message=$value->message;
			// 			$rules = array($message => 'max:160',$mobile => 'required');
	  //   				$validator = Validator::make($request->all(), $rules); 
			// 			$data = $request->all();
	
	  //   if ($validator->fails()) {
			// return response()->json(array(
			// 		'success' => false,
			// 		'message' => $validator->getMessageBag()->toArray()
			// 		));
	  //  }  
		
		// if (filter_var($data['email_address'], FILTER_VALIDATE_EMAIL) === false) {
		//      return response()->json(array(
		//             	'success' => false,
		// 				'message' => "Invalid Email address."
		// 				));
		// }
		 //    $mob="/^[6789][0-9]{9}$/";
			// if(!preg_match($mob,$mobile)
			// {	  return response()->json(array(
		 //            	'success' => false,
			// 			'message' => "Invalid Mobile number."
			// 			));
			// }
					
				 	if($message==null)
				 {
					
					$message = $stored_msg;
					
			      }
			
					$insert[] = ['mobile' => $mobile,'message' =>$message];

					

				}
				if(!empty($insert)){
					DB::table('items')->insert($insert);
					return response()->json(array(
	          		'success' => true,
					'message' => "Invalid Mobile number."
					));
				}
			}
		}
		return back();
	}
}