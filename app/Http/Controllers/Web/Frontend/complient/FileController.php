<?php
namespace App\Http\Controllers\Web\Backend\news;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;

use Validator;
use Image;
use DB;
class FileController extends Controller {
    // public function index(Request $request){
    //     $products = Product::orderBy('id','DESC')->paginate(5);
    //     return view('files.index',compact('products'))
    //         ->with('i', ($request->input('page', 1) - 1) * 5);
    // }
    // public function create(){
    //     return view('files.create');
    // }
   public function news_store(Request $request) {
        

         $rules = array('title' => 'required',
            'description' => 'required',
            'product_image' => 'required|image|mimes:jpeg,png,jpg|max:2048');
        $validator = Validator::make($request->all(), $rules); 
        $data = $request->all();
    
        if ($validator->fails()) {
            return response()->json(array(
                    'success' => false,
                    'message' => $validator->getMessageBag()->toArray()
                    ));
       }  
          

                $title= $request->get('title');
                 $state_id= $request->get('state_id');
                        
                $description =$request->get('description');
                $cat =$request->get('cat');
                $priority= $request->get('priority');
                
                if($priority!=1)
                {
                    $priority=0;
                }
               

if($cat==103)
{
  if ($state_id==0)
  {
     return response()->json(array(
                        'success' => false,
                        'message' => "Select the state "
                        ));

  }
  $insert[] = ['category_id'=>$cat,'state_id'=>$state_id,'title' => $title,'description'=>$description,'priority'=>$priority];
}
else
{
    $insert[] = ['category_id'=>$cat,'title' => $title,'description'=>$description,'priority'=>$priority];
}
            // $image = Input::file('product_image');
            // $filename  = time() . '.' . $image->getClientOriginalExtension();

            // $path = public_path('img' . $filename);
 
        
            //    Image::make($image->getRealPath())->resize(200, 200)->save($path);
                
            //   $img = $filename;
         
// save image to database

            

             

                    DB::table('news')->insert($insert);
         

       $data['id']=DB::getPdo()->lastInsertId();

       $allowed =  array('png', 'jpg');
        $filename = $_FILES['product_image']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) {
           return response()->json(array(
                                'success' => false,
                                'message' => "Extension error only  png or jpg valid "
                        ));
                }
       
        $img = $request->file('product_image');
        if($img!=null) {   
        $img_cat="img"; 

        $i_year=date('Y'); $i_month=date('m'); $image_name=uniqid(); $ext=$img->getClientOriginalExtension(); 
        if (!is_dir("img" . $img_cat)) {
            mkdir("img" . $img_cat);
        }
        
        if (!is_dir("img" . $img_cat . "/" . $i_year)) {
            mkdir( "img" . $img_cat . "/" . $i_year);
        }
        if (!is_dir("img" . $img_cat . "/" . $i_year . "/" . $i_month)) {
            mkdir("img" . $img_cat . "/" . $i_year . "/" . $i_month);
        }
        list($width, $height) = getimagesize($img);

                 $ratio = $width/$height; // width/height

                 if ($ratio > 1) {
                 $width = 150;
                 $height = 150/$ratio;
                 } else {
                 $width = 150*$ratio;
                 $height = 150;
                 }

       Image::make($img)->resize($width, $height)->save('img'.$img_cat.'/'.$i_year.'/'.$i_month.'/'. $image_name.'_s.'.$ext  );


    

                 if ($ratio > 1) {
                 $width = 600;
                 $height = 600/$ratio;
                 } else {
                 $width = 600*$ratio;
                 $height = 600;
                 }

       Image::make($img)->resize($width, $height)->save('img'.$img_cat.'/'.$i_year.'/'.$i_month.'/'. $image_name.'_l.'.$ext  );

      

 }
 $img=DB::insert('insert into img(news_id,category,year,month,image_name,ext) values(?,?,?,?,?,?)',array($data['id'],$img_cat,$i_year,$i_month,$image_name,$ext));

                    
                    return response()->json(array(
                    'success' => true,
                    'message' => "insert success."
                    ));
             
                
           
        // $product = new Product($request->input()) ;
     
        //  if($file = $request->hasFile('product_image')) {
            
        //     $file = $request->file('product_image') ;
            
        //     $fileName = $file->getClientOriginalName() ;
        //     $destinationPath = public_path().'/images/' ;
        //     $file->move($destinationPath,$fileName);
        //     $product->product_image = $fileName ;
        // }
        // $product->save() ;
         
    }


     public function news_job(Request $request) {
        

         $rules = array('title' => 'required',
            'description' => 'required');
        $validator = Validator::make($request->all(), $rules); 
        $data = $request->all();
    
        if ($validator->fails()) {
            return response()->json(array(
                    'success' => false,
                    'message' => $validator->getMessageBag()->toArray()
                    ));
       }  
          

                $title= $request->get('title');
                 $job_id= $request->get('job_id');
                        
                $description =$request->get('description');
                       
                       //echo $job_id;         
                
              if ($job_id==0)
              {
                 return response()->json(array(
                                    'success' => false,
                                    'message' => "Select the state "
                                    ));

              }
                $insert[] = ['job_id'=>$job_id,'title' => $title,'description'=>$description];
         
                   DB::table('jobpost')->insert($insert);
         




                    
                    return response()->json(array(
                    'success' => true,
                    'message' => "insert success."
                    ));
             
                
           
     
         
    }
    
}