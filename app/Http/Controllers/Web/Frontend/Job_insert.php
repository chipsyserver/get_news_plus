<?php
namespace App\Http\Controllers\Web\Frontend;
use App\Http\Controllers\Controller;
use  DB;
use Hash;
use Illuminate\Http\Request;
use Validator;
use File;
use Mail;
use App\Repository\Backend\DropdwonControl;
class Job_insert extends Controller {
   
    public function job_insert(Request $request){ 
	
		    $rules = array( 'name'=> 'required','mobile_number' => 'required','email'=>'required','address'=>'required','message'=>'required','resume' => 'required|mimes:doc,docx,odt|max:2048');
	    $validator = Validator::make($request->all(), $rules); 
		$data = $request->all();
	
	    if ($validator->fails()) {
			return response()->json(array(
					'success' => false,
					'message' => $validator->getMessageBag()->toArray()
					));
	   }  
		// $timestmp=time();
		if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
		     return response()->json(array(
		            	'success' => false,
						'message' => "Invalid Email address."
						));
			}

		    $mob="/^[6789][0-9]{9}$/";
			if(!preg_match($mob,$data['mobile_number']))
			{	  return response()->json(array(
		            	'success' => false,
						'message' => "Invalid Mobile number."
						));
			}


	
		  // DB::table('count')->increment('presentcount');
		  // return response()->json(array(
		  //           	'success' => true,
				// 		'message' => "message is finished."
				// 		));



		


	
// die(json_encode($count[0]->presentcount));
		   // $usercont = DB::table('count')->where($count[0]->presentcount,$count[0]->presentcount)->count();

	

		
		// else{
			$name= $request->get('name');
                 $mobile_number= $request->get('mobile_number');
                        
                $email =$request->get('email');
               $address =$request->get('address');
				 $message =$request->get('message');
				 // echo $message;
		 	  $insert[] = ['name'=>$name,'mobile'=>$mobile_number,'email' =>$email,'address'=>$address,'message'=>$message];
		 


		 $file = $request->file('resume');

       $allowed =  array('doc', 'docx','odt');
        $filename = $_FILES['resume']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) {
           return response()->json(array(
                                'success' => false,
                                'message' => "Extension error only  doc/docx  or odt  valid "
                        ));
                }
       
          
        DB::table('profile')->insert($insert);




		      $data['profile_id']=DB::getPdo()->lastInsertId();
        if($file!=null) {   
        $file_cat="file"; 

        $i_year=date('Y'); $i_month=date('m'); 
        $file_name=uniqid();
         $ext=$file->getClientOriginalExtension(); 
        if (!is_dir($file_cat)) {
            	mkdir($file_cat);
        }
        
        if (!is_dir($file_cat . "/" . $i_year)) {
            mkdir($file_cat . "/" . $i_year);
        }
        if (!is_dir($file_cat . "/" . $i_year . "/" . $i_month)) {
            mkdir($file_cat . "/" . $i_year . "/" . $i_month);
        }
 

      

      $file->move($file_cat.'/'.$i_year.'/'.$i_month.'/', $file_name.'.'.$ext  );


  

 }
 $file=DB::insert('insert into file(profile_id,category,year,month,file_name,ext) values(?,?,?,?,?,?)',array($data['profile_id'],$file_cat,$i_year,$i_month,$file_name,$ext));

             







		   return response()->json(array(
		            	'success' => true,
		            	'message' => "Your profile Send Successfully "
		 				));
		 }



		 public function job_insert1(Request $request){ 
	
		    $rules = array( 'name'=> 'required','mobile_number' => 'required','email'=>'required','address'=>'required','message'=>'required','resume' => 'required|mimes:doc,docx,odt|max:2048');
	    $validator = Validator::make($request->all(), $rules); 
		$data = $request->all();
	
	    if ($validator->fails()) {
			return response()->json(array(
					'success' => false,
					'message' => $validator->getMessageBag()->toArray()
					));
	   }  
		// $timestmp=time();
		if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
		     return response()->json(array(
		            	'success' => false,
						'message' => "Invalid Email address."
						));
			}

		    $mob="/^[6789][0-9]{9}$/";
			if(!preg_match($mob,$data['mobile_number']))
			{	  return response()->json(array(
		            	'success' => false,
						'message' => "Invalid Mobile number."
						));
			}


	
			$name= $request->get('name');
                 $mobile_number= $request->get('mobile_number');
                        
                $email =$request->get('email');
                
               $address =$request->get('address');
				 $message =$request->get('message');
				 // echo $message;
		  $file = $request->file('resume');

       $allowed =  array('doc', 'docx','odt');
        $filename = $_FILES['resume']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) {
           return response()->json(array(
                                'success' => false,
                                'message' => "Extension error only  doc/docx  or odt  valid "
                        ));
                }
       
       
        if($file!=null) {   
        $file_cat="file"; 

        $i_year=date('Y'); $i_month=date('m'); 
        $file_name=uniqid();
         $ext=$file->getClientOriginalExtension(); 
        if (!is_dir($file_cat)) {
            	mkdir($file_cat);
        }
        
        if (!is_dir($file_cat . "/" . $i_year)) {
            mkdir($file_cat . "/" . $i_year);
        }
        if (!is_dir($file_cat . "/" . $i_year . "/" . $i_month)) {
            mkdir($file_cat . "/" . $i_year . "/" . $i_month);
        }
 

      

      $file->move($file_cat.'/'.$i_year.'/'.$i_month.'/', $file_name.'.'.$ext  );


 
 }
 
 // echo $my_file;

// $subject="Job Application";
// $email="noreplay@getnewsplus.com";
	 $path='http://www.getnewsplus.com/'.$file_cat.'/'.$i_year.'/'.$i_month.'/'.$file_name.'.'.$ext ;			

		
 // echo $path;	

			$recipient = "chipsyinfo@gmail.com";

        // Set the email subject.
        $subject = "New contact from $name";
        $email_headers = "From: $name <$email>";
		$email_headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        // Build the email content.
        $message = '<html><body>';
		$message .= '<h1>Name: $name</h1>';
		$message .= '<h1>Mobile_number: $mobile_number</h1>';
		$message .= '<h1>Email: $email</h1>';
		$message .= '<h4>Address:$address</h1>';
		$message .= '<h4>Address:</h4><p>$message</p>';
		$message .= '<a href="$path">Click Here view Resume</a>\n';
		$message .= '</body></html>';
        
        if (mail($recipient,$subject, $message,$email_headers)) {
            
            return response()->json(array(
		            	'success' => true,
		            	'message' => "Your Information Send   Successfully "
		 				));
        } else {
            // Set a 500 (internal server error) response code.
            // http_response_code(500);
            return response()->json(array(
		            	'success' => false,
		            	'message' => "Oops! Something went wrong and we couldn't send your message."
		 				));
            }
				
		    

             







		
		 }
	
	}

         
		
  

 

	