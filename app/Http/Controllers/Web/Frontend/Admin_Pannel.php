<?php 
namespace App\Http\Controllers\Web\Frontend;
use App\Http\Controllers\Controller;
use App\Product;
use App\listnews;
use App\listjob;
use App\news;
use App\TimeControl;
use Auth;
use  DB;
use Illuminate\Http\Request;

class Admin_Pannel extends Controller {
	 	
		

public function get_fun($id)
{
	// echo $id;

	
$response= DB::table('news as n')
	         ->join('img as i', 'i.news_id', '=', 'n.news_id')
	         ->join('category as c', 'c.category_id', '=', 'n.category_id')
	          // ->join('city as s', 's.id', '=', 'n.state_id')   
	          ->where('n.category_id',$id)   
	        ->orderBy('n.news_id', 'desc')
	         ->paginate(15);
	        	         
	         return $response;
	}


public function find_fun($id)
{
 if($id!=0)
 {
 	$response= DB::table('news as n')
	         ->join('img as i', 'i.news_id', '=', 'n.news_id')
	         ->join('category as c', 'c.category_id', '=', 'n.category_id')
	          ->join('city as s', 's.id', '=', 'n.state_id')   
	          ->where('n.state_id',$id)   
	        ->orderBy('n.news_id', 'desc')
	         ->get();
 }
 else
 {
 	$response= DB::table('news as n')
	         ->join('img as i', 'i.news_id', '=', 'n.news_id')
	         ->join('category as c', 'c.category_id', '=', 'n.category_id')
	          ->join('city as s', 's.id', '=', 'n.state_id')
	          
	         
	        
	        ->orderBy('n.news_id', 'desc')
	         ->get();
 }
	        return $response;

}
	
	public function index()
	{
		
	$news1 = news::with('news_data','news_data1')->get();
	 //die(json_encode($news1));
	return view('Frontend.index' )
	->with(array('data'=>listnews::listnews()))
	->with(array('job'=>listjob::listjob()))
	->with(array("news1"=>$news1));
     

		}


	public function single_function($name,$id,$title)
	{

		
			$data=DB::table('news as n')
	         ->join('img as i', 'i.news_id', '=', 'n.news_id')
	         ->join('category as c', 'c.category_id', '=', 'n.category_id')
	     	->where('n.news_id',$id)
	     	
	        ->orderBy('n.news_id', 'desc')

		    ->get();
			 //$data[0]->news_id=$id;
		 //echo $data;
		//	$data = preg_replace("(-)", " ", $data);
	  
	     return view('Frontend.Single' )
	     ->with(array('data'=>$data));
	     
     

		}


public function city_function($id,$name)
	{

		$data= DB::table('mycity as n')
	        ->where('n.mycity_id',$id)
	        
	                  ->get();

	       
	  
	  
	     return view('Frontend.single_city' )
	     ->with(array('data'=>$data));
	     
     

		}

	public function complient()
	{
		
	return view('Frontend.compliant');

	}
	public function job()
	{
		
	return view('Frontend.job')->with(array('data'=>listnews::listnews()))
	->with(array('job'=>listjob::listjob()));

	}

	public function getjob()
	{
		
	
return listjob::listjob();
	}

	public function contact()
	{
		

		return view('Frontend.contact');
     

		}
	

public function category_function($name,$id,$title)
	{
		

	
             $data=DB::table('news as n')
	         ->join('img as i', 'i.news_id', '=', 'n.news_id')
	         ->join('category as c', 'c.category_id', '=', 'n.category_id')
	     	->where('n.category_id',$id)
	     	// ->where('logon', '=', $newdate)
	        ->orderBy('n.news_id', 'desc')

		    ->paginate(15);
			 //$data[0]->news_id=$id;
			// echo $data;
		//	$data = preg_replace("(-)", " ", $data);
	  
	     return view('Frontend.category' )->with(array('data'=>$data));
	}


		

	public function getmycity()
	{
		
           
			
	  
	     return view('Frontend.mycity' )->with(array('data'=>listnews::mycity()));


		}

		public function get_city(Request $request)
		{
			$query = $request->query();


if((int)$query['city']!=0 && (int)$query['cat'] !=0)
 {
 	 $mycity= DB::table('mycity as n')
	         ->join('img as i', 'i.mycity_id', '=', 'n.mycity_id')
	         ->join('place as c', 'c.id', '=', 'n.city_id')
	         ->join('shop_category as s', 's.shop_id', '=', 'n.shop_id')

	         ->where('n.city_id',(int)$query['city'])
	         ->where('n.shop_id',(int)$query['cat'])
	         
	        ->orderBy('n.mycity_id', 'desc')

           ->paginate(10);

		   return $mycity; 
	           
	          
 }

else if ((int)$query['city']==0 && (int)$query['cat'] ==0) {
	 return listnews::mycity();
}


 else
 {
 	$mycity= DB::table('mycity as n')
	         ->join('img as i', 'i.mycity_id', '=', 'n.mycity_id')
	         ->join('place as c', 'c.id', '=', 'n.city_id')
	         ->join('shop_category as s', 's.shop_id', '=', 'n.shop_id')

	         ->where('n.city_id',(int)$query['city'])
	         ->orWhere('n.shop_id',(int)$query['cat'])
	         
	        ->orderBy('n.mycity_id', 'desc')

           ->paginate(10);

		   return $mycity;

 }

 
	
		}
	
	public function get_mycity($id,$shop)
	{
 if($id!=0 && $shop !=0)
 {
 	 $mycity= DB::table('mycity as n')
	         ->join('img as i', 'i.mycity_id', '=', 'n.mycity_id')
	         ->join('place as c', 'c.id', '=', 'n.city_id')
	         ->join('shop_category as s', 's.shop_id', '=', 'n.shop_id')

	         ->where('n.city_id',$id)
	         ->where('n.shop_id',$shop)
	         
	        ->orderBy('n.mycity_id', 'desc')

           ->paginate(10);

		   return $mycity; 
	           
	          
 }
 elseif ($id!=0) {
 	$response=listnews::mycity1($id);  
 }
  elseif ($shop!=0) {
 	$response=listnews::shop($shop);  
 }
 else
 {
 	$response= listnews::mycity();
 	       }
	        return $response;

}
	// public function edit_func($id)
	// {
	// 	 $data =  DB::table('company as c')
 //            ->join('company_login as l',  'l.company_id',     '=', 'c.company_id')
	// 		->join('company_setting as s', 's.company_id',    '=', 'c.company_id')
	// 		->leftjoin('company_logo as lg', 'lg.company_id', '=', 'c.company_id')
	// 		->where('c.company_id',$id)
	// 	    ->get();
			
	//    return $data;
	// }
	


	
	
	 	

}
