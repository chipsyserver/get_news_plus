<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class job extends Model
{
    public $table = 'jobpost';
    public $fillable = ['news_id','job_id','title','description'];
    
   public function news_job()
	{

		return $this->belongsTo('App\job_category', 'job_id', 'job_id');
		
		}

}



