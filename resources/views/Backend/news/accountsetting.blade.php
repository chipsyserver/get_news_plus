
<!DOCTYPE html>
<html>
   @include('Backend.header.header')
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
 @include('Backend.header.menu')
         

                         

        
     
<div class="wrapper">

  
  <!-- Full Width Column -->
  <div class="content-wrapper">
<div class="container">
      <!-- Content Header (Page header) -->
     <!--  <div class="row"><h2 class="page-header col-sm-6">Company's</h2>
<div class="col-sm-6"><div id="example1_filter" class="dataTables_filter pull-right"><button type="button" class="btn bg-olive margin" onClick="location.href='/company/display';">Back</button></div></div></div> -->

      <!-- Main content -->
 
       <div class="row">
       <div class="col-md-12 contaner-createcompany">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Change the Password </h3>
            </div>
             <form role="form" name="account_fromdata" class="form-horizontal"  > <div class="box-body">
                <div class="form-group">
				<label for="name" class="control-label col-sm-2 right">User Name:</label>
				<div class="col-sm-6">
				<input type="text" name="uname" class="form-control" required="true" value="{{ Auth::user()->user_name  }}" autofocus>
				</div>
				</div>
		<div class="form-group check_changepassword">
		 <label for="asdf" class="control-label col-sm-2"> &nbsp;</label>
		 <div class="col-sm-6">
		<label class="checkbox">
		<input type="checkbox" name="change_password">Change Password</label>
		 </div>
		 </div>
		<div class="form-group">
		<label for="email" class="control-label col-sm-2 right">Old Password</label>
		<div class="col-sm-6">
		<input type="password" name="old_password" class="form-control pass_control"  required="true">
		</div>
		</div>
        <div class="form-group ">
		</div>
        <div class="form-actions">
      
       <div class="form-group">
        <div class="box-footer col-md-2 col-md-offset-2 ">
                 <input type="button" class="btn btn-satgreen" value="Update" name="submitaccount">
                </div>
                 <div class="display_message col-md-6" style="padding-top: 10px; font-weight:bold"></div> 
                </div>
               </div> 
               </div>
            </form>
            
          </div>
     </div>

        </div>
       
       </div>
    
    </div>
    
    </div>
  <!-- /.content-wrapper -->

</div>


<!-- ./wrapper -->
@include('Backend.footer.footer')
 <button type="button" class="load_btns addloading" data-target="body"  style="display:none"> </button>
 <button type="button" class="load_btns loaddiv" data-target="self" style="display:none"></button>
 <button type="button" class="load_btns removeloading" data-target="close" style="position: relative;z-index: 11000;display:none;"></button>
<!-- jQuery 2.2.3 -->
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>


<!-- <script src="//code.jquery.com/jquery-2.1.3.min.js"></script> -->
    <!-- <script src="https://raw.githubusercontent.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script> -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>

 


 <script src="/js/animatescroll.js"></script>
<script src="/js/jquery-loader.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.js"></script>

<script src="/alert/js/alert.min.js"></script>

<!-- AdminLTE App -->
<script src="/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script src="/js/jquery.twbsPagination.js"></script>
<script src="/js/ckeditor/ckeditor.js"></script>

<script src="/js/account_setting.js"></script>
<!--thems-->


<script>

$(function () {
      $('.load_btns').on('click', function () {   
        $data = {
                autoCheck: $('#autoCheck').is(':checked') ? 32 : false,
                size: $('#size').val(),  
                bgColor: $('#bgColor').val(),  
                bgOpacity: $('#bgOpacity').val(), 
                fontColor: $('#fontColor').val(),  
                title: $('#title').val(),
                isOnly: !$('#isOnly').is(':checked')
            };
      
          switch ($(this).data('target')){
                case 'body':{
                    $.loader.open($data);
                    break;
        }
        case 'self':
                    $('.table-bordered').loader($data);
                    break;
                case 'close':
                    $.loader.close(true);
          break;

            }   
            
        });
});
</script>

</body>
</html>