@extends('Backend.layouts.master_layout')
@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('/js/parsley/parsley.css')}}">
<link rel="stylesheet" href="/css/jquery-confirm.min.css">



    <form class="" name="mycity" data-validate="parsley" action="#">
   <section class="panel panel-default" id="up">
      <header class="panel-heading"> <span class="h4">What is in your  city</span> </header>
      <div class="panel-body">
        <!--  <p class="text-muted">Need support? please fill the fields below.</p> -->
         
         <div class="form-group pull-in clearfix">
         <div class="col-sm-10 col-sm-offset-1"> 
          <div class="col-sm-6"> <label>Select Place</label>
      

       <select class="form-control m-t parsley-validated mySelect"  required="required" id="mySelect" name="city">
          <option  required="required" class="state"   value='' > Select the Place</option>                 
         @foreach(App\Place::get() as $city)
         <option class="state"   value='{{ $city->id }}' > {{ $city->city}}</option>

         @endforeach
      </select>
     
       </div>
       <div class="col-sm-6"> <label>Select Shop Category</label>
      
      <select  required="required" name="category" class="form-control m-t parsley-validated">
       <option  required="required" class="state"   value='' > Select the Shop Category</option>
        @foreach(App\shop_category::get() as $shop)
         <option  required="required" class="state"   value='{{ $shop->shop_id }}' > {{ $shop->shop_category}}</option>

         @endforeach
          
      </select>
     
   </div>



                            <div style="padding-top: 10px"> 

            <div class="col-sm-6 " style="margin-top: 10px;"> <label>Shop name</label> <input type="text" class="form-control parsley-validated" id="name" name="name" placeholder="Name"  required="required"> </div>

            <div class="col-sm-6" style="margin-top: 10px;"> <label>Mobile</label> <input type="text" class="form-control parsley-validated" name="phone" id="phone" placeholder="Enter mobile Number" data-parsley-maxlength="10" data-parsley-pattern="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$" data-parsley-pattern-message="Number is invalid "  required="required"> </div>
</div>
            <div class="col-sm-6" style="margin-top: 10px;"> <label>Land Line</label> <input type="text" class="form-control parsley-validated"  data-parsley-type="digits" data-parsley-type-message="only numbers" id="landline" name="landline" placeholder="phone number"  > </div>
            <div class="col-sm-6" style="margin-top: 10px;"> <label>Email</label>   <input type="email"  name="email" id="email" class="form-control" placeholder="Email" data-parsley-errors-container="#email_format_error"> </div>
         
<button type="button" id="show" style='visibility:hidden;margin-top: 10px;'  >
                     <b style="color: #187bbb;font-size: 15px;padding-left: 10px;">Do you Want to change the Image Click Here</b> </button>
                                <!-- <button id="hide">Hide</button> -->
                          <div class="form-group img">
                                    <label class="col-sm-2 control-label"> Select Image</label> 
                                    <div class="col-sm-10"> 
                                    

                          <input type="file" class="form-control" name="product_image" id="product_image"  placeholder="Select Log"  >
                                 </div>
                                 </div>

                                

         <div class="col-sm-12" style="margin-top: 10px;"> <label>Descriptions</label> <textarea class="form-control parsley-validated" rows="7" name="description" id="description" data-minwords="6"  required="required" placeholder="Type your message"></textarea> </div>
      </div>
      </div>
      <footer class="panel-footer text-right bg-light lter"> 
         <div class="row">
         <div class="col-md-1 col-md-offset-2">      

                                     
<button type="button" name="btnsubmit" id="btnsubmit"   class="btn btn-primary btn-block btn-flat" style='visibility:show'  >Save</button>
<button type="button" name="btnupdate" id="btnupdate"  class="btn btn-info btn-block btn-flat" value=" " style='visibility:hidden;' >Update</button>
</div>
<div class="col-md-1 ">  
<button type="button" name="btncancel" id="btncancel"   class="btn btn-default btn-block btn-flat"   >Cancel</button>

</div>
</div> </footer>
   </section>
</form>

                      

 <div class="line line-dashed b-b line-lg pull-in"> </div>


</div>
</section>
<section id="content">
               <!--    <section class="vbox"> -->
                     <section class="scrollable padder" id="down">
                        <div class="m-b-md">
                           <h3 class="m-b-none">NEWS</h3>
                        </div>
                        <section class="panel panel-default">
                          <!--  <header class="panel-heading"> News <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> </header> -->
                           <div class="table-responsive">
                              <table id ="#page-content " class="table table-striped m-b-none  table-hover" ">
                                 <thead>
                                    <tr align="center">
                                    <th width="2%">ID</th>
                                     <th width="8%">Image</th>
                                    <th width="5%">Name</th>
                                     <th width="5%">Mobile</th>
                                   <th width="5%">email</th>
                                   <th width="10%">Descriptions</th>
                                   <th>city</th>
                                   <th>Shop_Category
                                    <th width="10%">Action</th>
                                                                           </tr>
                                 </thead>
                                 <tbody class="company-list"> 
              
                       </tbody>
              
             
               </table>
                       <div class="box-footer clearfix companypage">
              <ul id="pagination-demo" class="pagination-lg pagination pagination-sm no-margin pull-left">
         
              </ul>
            </div>
            </div>
                           </div>
                        </section>
                     </section>
                <!--   </section> -->
                  <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a> 
 <br/><br/>
                  
               </section>

            


                                
                                  


</div>
                  

     



                                      
</section>
       <!-- </div>
      
    </div> -->
    <script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- AdminLTE for demo purposes -->

<script src="/js/jquery-confirm.min.js"></script>
<script src="/js/jquery.twbsPagination.js"></script>
<script src="/chosen/chosen.jquery.min.js"></script>
<script src="/js/jquery-loader.js"></script>

<script src="{{asset('js/parsley/parsley.js')}}"></script>
<!-- <script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script> -->
  <script src="/js/mycity.js"></script> 
<script type="text/javascript">
var page = "<?php $i = 1;  $page=ceil($mycity->total()/5); echo $page; ?>";

   
  $('#pagination-demo').twbsPagination({

                totalPages: page,
                visiblePages: 6,
                onPageClick: function(event, page) {
                  
$.mycity.display(page);
                  
                }
            });
</script>

  
     <script src="/js/ckeditor/ckeditor.js"></script>


<script type="text/javascript">
     CKEDITOR.replace( 'description',
      {
        filebrowserBrowseUrl :'/js/ckeditor/js/ckeditor/filemanager/browser/default/browser.html?Connector=includes/js/editor_files/js/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl : '/js/ckeditor/js/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=includes/js/editor_files/js/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl :'/js/ckeditor/js/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=includes/js/editor_files/js/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl  :'/js/ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl : '/js/ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl : '/js/ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
      });


</script> 
  
   @endsection