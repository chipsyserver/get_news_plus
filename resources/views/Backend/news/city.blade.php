@extends('Backend.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('/js/parsley/parsley.css')}}">
<link rel="stylesheet" href="/css/jquery-confirm.min.css">
 <script src="{{asset('Assets/angular/angular.js')}}"></script>
    <script src="{{asset('Assets/angular/route.js')}}"></script>
    <script src="{{asset('Assets/angular/ui-bootstrap-tpls-2.5.0.min.js')}}"></script>

<section id="content" ng-controller="NewsController">
               <!--    <section class="vbox"> -->
                     <section class="scrollable padder" id="down">
                        <!-- <div class="m-b-md">
                           <h3 class="m-b-none">NEWS</h3>
                        </div> -->
                        <section class="panel panel-default">
                          <!--  <header class="panel-heading"> News <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> </header> -->
                         
            <div class="col-sm-6 col-sm-offset-2">
   <section class="panel panel-default">
      <header class="panel-heading"> Place</header>
      <table class="table table-striped m-b-none">
         <thead>
            <tr>
               <th>ID</th>
               <th>Place</th>
               <th width="70">Controls</th>
            </tr>
         </thead>
         <tbody class="company-list">
          <?php $i=1; ?>
         
                
            <tr ng-repeat="mycity in visitors" ng-cloak >
               <td width="2%"><%(visitorsPerPage * (currentPage-1)) + $index+1  %></td>
               <td>

               <input  type="text" name="city" id="<%mycity.id%>" class="form-control" 
                        value="<% mycity.city %>" ng-disabled="isDisabled" 
            ng-click="save()" />
                        
               </td>
               <td >
                  
              
              <a href="#"  id="<%mycity.id%>" ng-if="showBtns" ng-hide="showSave " name="edit" ng-click="edit($event)"> <i class="fa fa-pencil " style="font-size: 20px"></i></a> 
                   <a href="#" id="<%mycity.id%>" ng-if="showBtns" ng-show="showSave"   ng-click="update($event)">


                   <i class="fa  fa-check-square-o" style="font-size: 20px"></i></a>
                 
                 


                     <a href="#"  id="<% mycity.id %>" ng-if="showBtns" ng-hide="showSave " data-ng-click="delete($event)" class=""   ><i class="fa  fa-trash-o " style="font-size: 20px"></i></a>






                   <a href="#"  ng-if="showBtns" ng-show="showSave"   ng-click="cancel()">


                   <i class="fa   fa-times" style="font-size: 20px"></i></a>




                     
                 
               </td>

            </tr>
  			
            <tr>
            <td>#</td>
            <td><input type="text" class="form-control" name="city" id="city" required="true"> </td>
            <td>  <a href="#" class="" name="addcity" id="addcity" ><i class="i i-plus2" style="font-size: 20px"></i></a>  </td>
            </tr>
          
         </tbody>
      </table>
   </section>
</div>
                           </div>
                        </section>
                     </section>
                <!--   </section> -->
                  <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a> 
 <br/><br/>
                  
               </section>

            


                                
                                  


</div>
                  

     



                                      
</section>
       <!-- </div>
      
    </div> -->
    <script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- AdminLTE for demo purposes -->

<script src="/js/jquery-confirm.min.js"></script>
<script src="/js/jquery.twbsPagination.js"></script>
<script src="/chosen/chosen.jquery.min.js"></script>
<script src="/js/jquery-loader.js"></script>

<script src="{{asset('js/parsley/parsley.js')}}"></script>
<!-- <script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script> -->
  <script src="/js/city.js"></script> 
<!-- <script type="text/javascript">
var page = "<?php $i = 1;  $page=ceil($data->total()/5); echo $page; ?>";

   
  $('#pagination-demo').twbsPagination({

                totalPages: page,
                visiblePages: 6,
                onPageClick: function(event, page) {
                  
$.mycity.display(page);
                  
                }
            });
</script> -->

  
      
  
   @endsection