
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- AdminLTE for demo purposes -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script src="/js/jquery.twbsPagination.js"></script>
<script src="/js/news.js"></script>
<script type="text/javascript">
var page = "<?php $i = 1;  $page=ceil($data->total()/10); echo $page; ?>";

     
  $('#pagination-demo').twbsPagination({

                totalPages: page,
                visiblePages: 6,
                onPageClick: function(event, page) {

                       $.news.display(page);
                  
                }
            });
</script>

<script src="/js/ckeditor/ckeditor.js"></script>


<script type="text/javascript">
     CKEDITOR.replace( 'description',
      {
        filebrowserBrowseUrl :'/js/ckeditor/js/ckeditor/filemanager/browser/default/browser.html?Connector=includes/js/editor_files/js/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl : '/js/ckeditor/js/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=includes/js/editor_files/js/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl :'/js/ckeditor/js/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=includes/js/editor_files/js/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl  :'/js/ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl : '/js/ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl : '/js/ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
      });


</script> 