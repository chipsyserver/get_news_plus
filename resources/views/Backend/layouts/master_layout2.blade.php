<!DOCTYPE html>
<html>
   @include('Backend.header.header')
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
 @include('Backend.header.menu')
                         

        
     
<div class="wrapper">

  
  <!-- Full Width Column -->
  <div class="content-wrapper">
    
         <!-- <hr> -->
         
       @yield('content')

    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->

</div>


<!-- ./wrapper -->
@include('Backend.footer.footer')
 <button type="button" class="load_btns addloading" data-target="body"  style="display:none"> </button>
 <button type="button" class="load_btns loaddiv" data-target="self" style="display:none"></button>
 <button type="button" class="load_btns removeloading" data-target="close" style="position: relative;z-index: 11000;display:none;"></button>
<!-- jQuery 2.2.3 -->
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>


<!-- <script src="//code.jquery.com/jquery-2.1.3.min.js"></script> -->
    <!-- <script src="https://raw.githubusercontent.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script> -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>

 


 <script src="/js/animatescroll.js"></script>
<script src="/js/jquery-loader.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.js"></script>

<script src="/alert/js/alert.min.js"></script>

<!-- AdminLTE App -->
<script src="/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script src="/js/jquery.twbsPagination.js"></script>
<script src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
var page = "<?php $i = 1;  $page=ceil($data->total()/10); echo $page; ?>";

     
   $('#pagination-demo').twbsPagination({

                totalPages: page,
                visiblePages: 6,
                onPageClick: function(event, page) {

                       $.news.display(page);
                    // if (!$('page').last().hasClass('active')) {
                    //     var nxt = $('.pagination').find('li.active').find('a').html();
                    //     nxt++;
                    //     $('.pagination').find('li.active').removeClass('active');
                    //     $('.pagination').find('li.page_number#' + nxt).addClass('active');
                      
                    // }
                }
            });
</script>
<script src="/js/pagination.js"></script>

<script src="/js/account_setting.js"></script>
<!--thems-->


<script>

$(function () {
      $('.load_btns').on('click', function () {   
        $data = {
                autoCheck: $('#autoCheck').is(':checked') ? 32 : false,
                size: $('#size').val(),  
                bgColor: $('#bgColor').val(),  
                bgOpacity: $('#bgOpacity').val(), 
                fontColor: $('#fontColor').val(),  
                title: $('#title').val(),
                isOnly: !$('#isOnly').is(':checked')
            };
      
          switch ($(this).data('target')){
                case 'body':{
                    $.loader.open($data);
                    break;
        }
        case 'self':
                    $('.table-bordered').loader($data);
                    break;
                case 'close':
                    $.loader.close(true);
          break;

            }   
            
        });
});
</script>

</body>
</html>
