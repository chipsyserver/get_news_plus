@extends('Frontend.layouts.master_layout')
@section('content')
<!--breadcrumb-->
     <style type="text/css">
.column-post  img{
    max-width: 100%;
    height: 250px;
    object-fit: cover;
}
</style>
<section class="breadcrumb">
   <div class="container">
      <ol class="cm-breadcrumb">
         <li><a href="#">Home</a></li>
       
           @foreach ($data->slice(0, 1) as $news)
         <li class="cm-active">{{$news->title}}</li>
          @endforeach

      </ol>
   </div>
</section>
<!--end breadcrumb-->
<!--content-->
<section class="content item-list">
   <div class="container">
      <div class="row">
         <div class="col-md-9 page-content-column">
            <div class="single-post clearfix">
               <div class="topic">
                @foreach ($data as $news)
                 <?php

                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                        $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name}"));

                        ?>
                  <a href="/category/{{$category_name}}/{{$news->category_id}}/{{$title}}" class="tag bg1">{{$news->category_name}}</a>
                  <h3>{{$news->title}}</h3>
                  <ul class="post-tools">
                     <li> by <a href=""><strong>GetNewsPlus</strong> </a></li>
                     <li>  {{App\TimeControl::time_ago($news->created_at)}} </li>
                     <!-- <li><a href=""> <i class="ti-thought"></i> 57</a> </li> -->
                  </ul>
               </div>
               <!-- <div class="flex-slider2">
                  <div class="flexslider">
                     <ul class="slides">
                        <li>                                 
                           <a href="#" class="l-news-slider">
                           <img src="img/post/s1.jpg" alt="" class="img-responsive">   
                           </a>  
                        </li>
                        <li>
                           <a href="#" class="l-news-slider">
                           <img src="img/post/s3.jpg" alt="" class="img-responsive">   
                           </a>  
                        </li>
                        <li>
                           <a href="#" class="l-news-slider">
                           <img src="img/post/s4.jpg" alt="" class="img-responsive">  
                           </a>  
                        </li>
                     </ul>
                  </div>
                  <span class="tg-postfeatured"><i class="fa fa-bolt"></i></span>
               </div> -->
               <!--flex slider-->
               <div class="post-text">
        
                  <?php 
                                    echo "{$news->description}";
                                  
                                   ?>
               </div>
               <img src="img/bg-1.jpg" class="img-responsive" alt="">
               <br>
               @endforeach
             
         </div>
        </div>

@include('Frontend.sidebar.sidebar')
              
           </div>
       </div>
</section>
    <!-- content end-->
   @endsection