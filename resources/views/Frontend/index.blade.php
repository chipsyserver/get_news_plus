@extends('Frontend.layouts.master_layout')
@section('content')
      <!--content-->
   <section class="content">
       <div class="container">
           <div class="row">
               <div class="col-sm-9 col-md-9 ">
                  <div class="content-col">
                     <!--breaking slide-->
                      <div class="breaking-news-slide clearfix">
                          <h5 class="bg1 ">Breaking News</h5>
                          <div class="newsslider ">
                              <ul class="slides">
                               @foreach ($data->slice(0, 4) as $news)
                                <?php

                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                        $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name}"));


                        ?>
                                  <li class="cen" style=" "> <a  href="single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}">
                                  <?php 
                                   
                                    echo str_limit("{$news->title}", $limit = 50, $end = '...')
                                   
                                  
                                   ?>

                                  </a> </li>

                                  @endforeach
                                  
                                 <!--  <li> <a href="#">The World’s Youngest Billionaire is Just 24...</a> </li>
                                  <li> <a href="#">Stewart had a really good pattern for a handbag Just 24….</a> </li> -->
                              </ul>
                          </div>
                      </div>
                      <!--breaking slide end-->

                      <!--flex slider-->
                      <div class="flex-slider2">
                          <div class="flexslider">
                              <ul class="slides">
                                  @foreach ($data->slice(0, 4) as $news)
                                   <?php

                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                        $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name}"));

                        ?>
                                  <li>

                                      <a href="single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}" class="news-slider"> 
                                      <img src="/img{{$news->category }}/{{$news->year }}/{{$news->month }}/{{$news->image_name }}_l.{{$news->ext }}" class="img-responsive" alt="Image" style="" 
                                      >
                                          <div class="news-overlay"> 

                                             <h3>{{$news->title}}</h3>

                                              <span class="date">
                                  {{App\TimeControl::time_ago($news->created_at)}}
                                              </span>
   <!--           <span class="view"><i class="fa fa-eye"></i> 100</span>
    <span class="comments"><i class="fa fa-comment"></i> 15</span>  -->
                                          </div>
                                      </a>


                                  </li>
                                  @endforeach
                                 
                              </ul>
                          </div>
                      </div>
                      <!--flex slider end-->

                      <!--latest post-->
                      
                     

                     
<!--short news -->
                      
                      <div class="short-post">
                          <h3 class="main-title">Local News</h3>
                          <div class="row">
                           @foreach (App\listnews::news_data1(103,6) as $news)

                          
                         

                          <?php
                     // die(json_encode( App\news::with('news')->get()));
                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                        $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name}"));

                        ?>
                          
                              <div class="col-sm-6 wow animated fadeInUp" data-wow-delay="0.2s">
                                  <div class="media">
                                      <div class="media-left">


                                       <a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}"  class="img-thumbnail">
                                 <img src="/img{{$news->category  }}/{{$news->year}}/{{$news->month }}/{{$news->image_name}}_l.{{$news->ext}}" class="media-object" width="150" height="85" alt="Image">
                                                                  </a>
                                          
                                      </div>
                                      <div class="media-body">
                                         <h4 class="media-heading"><a href="single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}" >

                                 <?php 
                                    
                                    echo str_limit("{$news->title}", $limit = 22, $end = '...')
                                                                    
                                   ?>
                                   </a></h4>
                                 <!--    <h4><a href="#"</a></h4> -->

                                            <ul class="post-tools">
                                       <li> by <a href=""><strong> Get News</strong> </a></li>
                                        <li> 


                                          {{App\TimeControl::time_ago($news->created_at)}}
                                         </li>
                                   
                                    </ul>
                                      
                                      </div>
                                  </div>
                                  </div>
                                  @endforeach
                                  



                  </div>
            </div>





                     <!--latest post-->
                    
                            
                             
                        <?php $count = 0; ?>
                      @foreach ( App\listnews::news_data1(104,5) as $news)

                          
                         

                          <?php
                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                        $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name }"));

                        ?>
                          @if ($count =='0')
                           <!--latest post-->
                     <div class="news-by_category">
                        <h3 class="main-title">State News</h3>
                        <div class="row">
                         <?php $count = 0; ?>
                    
                         
                                 

                           <div class="col-sm-6 wow animated fadeInUp" data-wow-delay="0.2s">
                              <div class="column-post">


                               

                                <a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}">

                            <img src="/img{{$news->category  }}/{{$news->year}}/{{$news->month }}/{{$news->image_name}}_l.{{$news->ext}}" class="img-responsive" style="height: 225px;
    width: 395px;">
                                    <!--  <img  src="Frontend/img/post/l2.jpg" width="150" alt="..."> --> </a>    
                                  <div class="topic" style="    margin-top: 10px;">

                                    <span class="tag bg3">

                               <a href="/category/{{$category_name}}/{{$news->category_id}}/{{$title}}" class="tag bg1">{{$category_name}}</a></span>
                            <h4 class="media-heading"><a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}">{{$news->title}}</a></h4>
                            <?php 
                                      
                                      $s= strip_tags("{$news->description}");
                                    echo str_limit($s, $limit = 150, $end = '...');
                                   
                                  
                                   ?>
                                <p></p>
                                    <ul class="post-tools"">
                                       <li> by <a href=""><strong> Get News</strong> </a></li>
                                       <li>   {{App\TimeControl::time_ago($news->created_at)}} </li>
                                     <!--   <li><a href=""> <i class="ti-thought"></i> 57</a> </li> -->
                                    </ul>
                                 </div>
                              </div>
                          
                        </div>

                             
                              @endif
                               
                           
                           @if ($count !='0')
                            
                           <div class="col-sm-6 wow animated fadeInUp" data-wow-delay="0.4s">
                              <div class="media">
                                 <div class="media-left">
                                    <a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}">

<img src="/img{{$news->category  }}/{{$news->year}}/{{$news->month }}/{{$news->image_name}}_l.{{$news->ext}}" class="media-object" alt="Image" width="150" height= "100">
                                     </a>
                                 </div>
                                 <div class="media-body">

                                 <h4 class="media-heading"><a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}"> <?php 
                                    
                                    echo str_limit("{$news->title}", $limit = 22, $end = '...')
                                                                    
                                   ?>

                                  
                                 </a></h4>
                                 <?php 
                                   
                                
                                    $s= strip_tags("{$news->description}");
                                    echo str_limit($s, $limit = 50, $end = '...');
                                   
                                  
                                   ?>
                                  <p></p>
                                    <ul class="post-tools">
                                       <li> by <a href=""><strong> GetNewsPlus</strong> </a></li>
                                       <li>   {{App\TimeControl::time_ago($news->created_at)}} </li>
                                       <!-- <li><a href=""> <i class="ti-thought"></i> 57</a> </li> -->
                                    </ul>
                                 </div>
                              </div>
                              <!--media-->
                             
                        </div>
                       @endif
                       <?php $count++; ?> 
                      
                        @endforeach
                     </div>
                     </div>
                      
                     <!--latest post end-->
                      <!--world news-->
                     <div style="height: 40px;"></div>
                <div class="news-carousel wow animated fadeInUp" data-wow-delay="0.2s">

                        <h3 class="main-title"> National News </h3>
                        
                        <div id="post-carousel" class="owl-carousel">
                                   
                        <?php $count = 0; ?>
                      @foreach ( App\listnews::news_data1(101,5) as $news)

                          
                         

                          <?php
                     // die(json_encode( App\news::with('news')->get()));
                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                        $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name }"));

                        ?>
                          
                            <div>
                          
                              <div class="column-post">
                                 <a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}"  class="img-thumbnail">
                                 <img src="/img{{$news->category  }}/{{$news->year}}/{{$news->month }}/{{$news->image_name}}_l.{{$news->ext}}" class=" img-cover-dis"  alt="Image">
                                                                  </a>
                                 <div class="topic">
                              <?php

                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                         $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name }"));

                        ?>
                           <a href="category/{{$category_name}}/{{$news->category_id}}/{{$title}}" class="tag bg1">{{$category_name}}</a>

                             <h4 class="media-heading"><a href="single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}">

                                 <?php 
                                    
                                    echo str_limit("{$news->title}", $limit = 22, $end = '...')
                                                                    
                                   ?>
                                   </a></h4>
                                 <!--    <h4><a href="#"</a></h4> -->
                                    <p>       <?php 
                                   
                                
                                    $s= strip_tags("{$news->description}");
                                    echo str_limit($s, $limit = 50, $end = '...');
                                   
                                  
                                   ?> </p>
                                    <ul class="post-tools">
                                       <li> by <a href=""><strong> GetNewsPlus</strong> </a></li>
                                       <li> {{App\TimeControl::time_ago($news->created_at)}}</li>
                                       <<!-- li><a href=""> <i class="ti-thought"></i> 57</a> </li> -->
                                    </ul>
                                 </div>
                              </div>
                               
                       
                        </div>
                       
                            @endforeach
                           <!--item-->

                         
                          
                        </div>
                       
                        <!--post slider-->
                     </div>
                      <!--world news end-->

                     <!--latest post-->
                    
                            
                             
                        <?php $count = 0; ?>
                      @foreach ( App\listnews::news_data1(100,5) as $news)

                          
                         

                          <?php
                     // die(json_encode( App\news::with('news')->get()));
                        $title=preg_replace('/[ ,]+/', '-', trim("{$news->title}"));
                        $category_name=preg_replace('/[ ,]+/', '-', trim("{$news->category_name}"));

                        ?>
                          @if ($count =='0')
                           <!--latest post-->
                     <div class="news-by_category">
                        <h3 class="main-title">International News</h3>
                        <div class="row">
                         <?php $count = 0; ?>
                    
                         
                                 

                           <div class="col-sm-6 wow animated fadeInUp" data-wow-delay="0.2s">
                              <div class="column-post">


                               

                                <a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}">

                            <img src="/img{{$news->category  }}/{{$news->year}}/{{$news->month }}/{{$news->image_name}}_l.{{$news->ext}}" class="img-responsive" style="height: 225px;
    width: 395px;">
                                    <!--  <img  src="Frontend/img/post/l2.jpg" width="150" alt="..."> --> </a>          
                                     <div class="topic" style="    margin-top: 10px;">

                                    <span class="tag bg3">

                               <a href="/category/{{$category_name}}/{{$news->category_id}}/{{$title}}" class="tag bg1">{{$category_name}}</a></span>
                            <h4 class="media-heading"><a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}">{{$news->title}}</a></h4>
                            <?php 
                                   
                                    $s= strip_tags("{$news->description}");
                                    echo str_limit($s, $limit = 150, $end = '...');
                                   
                                  
                                   
                                  
                                   ?>
                                   <p></p>
                                    <ul class="post-tools">
                                       <li> by <a href=""><strong> GetNewsPlus</strong> </a></li>
                                       <li>   {{App\TimeControl::time_ago($news->created_at)}} </li>
                                     <!--   <li><a href=""> <i class="ti-thought"></i> 57</a> </li> -->
                                    </ul>
                                 </div>
                              </div>
                          
                        </div>

                             
                              @endif
                               
                           
                           @if ($count !='0')
                            
                           <div class="col-sm-6 wow animated fadeInUp" data-wow-delay="0.4s">
                              <div class="media">
                                 <div class="media-left">
                                    <a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}">

<img src="/img{{$news->category  }}/{{$news->year}}/{{$news->month }}/{{$news->image_name}}_l.{{$news->ext}}" class="media-object" alt="Image" width="150" height= "100">
                                    <!--  <img  src="Frontend/img/post/l2.jpg" width="150" alt="..."> --> </a>
                                 </div>
                                 <div class="media-body">

                                 <h4 class="media-heading"><a href="/single_page/{{$category_name}}/{{$news->news_id}}/{{$title}}"><?php 
                                    
                                    echo str_limit("{$news->title}", $limit = 22, $end = '...')
                                                                    
                                   ?>

                                  
                                 </a></h4>
                                <?php 
                                   
                                
                                    $s= strip_tags("{$news->description}");
                                    echo str_limit($s, $limit = 50, $end = '...');
                                   
                                  
                                   ?> <p></p>
                                  
                                     <ul class="post-tools">
              <li> by <strong> GetNewsPlus</strong> </li>
              <li> {{App\TimeControl::time_ago($news->created_at)}}</li>
                                       <!-- li><a href=""> <i class="ti-thought"></i> 57</a> </li> -->
                                    </ul>
                                 </div>
                              </div>
                              <!--media-->
                             
                        </div>
                       @endif
                       <?php $count++; ?> 
                      
                        @endforeach
                     </div>
                     </div>
                      
                     <!--latest post end-->
                     


                     
                     
                    
                            
                     

                      <!--Advertisement-->
                      <div style="height: 40px;"></div>
                      <div class="advertisement text-center clearfix">
                        <a href="#" target="blank">
                        <img class="img-responsive" src="/Frontend/img/ad/a3.png" alt="ad">
                     </a>
                      </div>
                      

                      
            </div>
            </div>
                 @include('Frontend.sidebar.sidebar')
              
           </div>
       </div>
</section>

    <!-- content end-->
   @endsection