@extends('Frontend.layouts.master_layout')
@section('content')
<!--content-->

<section class="content item-list">
   <div class="container">
      <div class="row">
         <div class="col-md-12 page-content-column">
            <h4 class="page-title">Compliant</h4>
            <div class="row">
               <div class="col-sm-8 col-md-8">
                  <div class="">
                     <form id="complient" name="complient">
                        <div class="form-group">
                           <input type="text" name="name" class="form-control" placeholder=" Name" required="true">
                        </div>
                        <div class="form-group">
                        <input type="text"  class="form-control" maxlength="10" name="mobile_number"  placeholder="Enter Mobile Number" required="true" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
                        </div>
                        <div class="form-group">
                           <input type="email" name="email" class="form-control" placeholder="Email"  required="true">
                        </div>
                        <div class="form-group">
                           <input type="text" name="address" class="form-control" placeholder="Address"  required="true">
                        </div>
                        <div class="form-group">
                           <textarea class="form-control" name="complient" id="complient" placeholder="complient" rows="5"  required="true"></textarea>
                        </div>
                        <div class="form-group">
                        <div class="col-sm-2">
          <button type="button" name="btncomplient" id="btncomplient"   class="btn btn-primary btn-block btn-flat" >Save</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
               <!-- <div class="col-sm-4 col-md-4 contact-details">
                  <p>Maecenas mauris elementum, est morbi at elite imperdiet libero  mauris elementum elite imperdiet libero.</p>
                  <p>124, lorem street, <br>Gwatemala, 302021</p>
                  <p><strong>Phone: </strong>+01 4593954 5959</p>
                  <p><strong>Fax: </strong>+01 34534 5959</p>
                  <p><strong>Email:</strong><a href="#">example@gmail.com</a></p>
               </div> -->
            </div>
         </div>
      </div>
      <div style="height: 40px;"></div>
   </div>
</section>
<!-- end content-->
   @endsection