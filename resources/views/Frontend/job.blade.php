@extends('Frontend.layouts.master_layout')
@section('content')


    <!--breadcrumb-->
    <section class="breadcrumb">
        <div class="container">
          <ol class="cm-breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="cm-active">Author Detail</li>
          </ol>
        </div>
    </section>
    <!--end breadcrumb-->

    <!--content-->
    <section class="content author-post">
        <div class="container">
            <div class="row">
                <div class="col-md-9 page-content-column">
                  
                  <h4> Please Send Resume for Suitable Job  for click 
        <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal1" style="margin-bottom:  10px" >Apply</a></h4>
                           
                    @foreach ($job as $job)
                    <div class="company-list">
                    <div class="tg-author  ">
                       <div class="tg-authorbox">
                            <!-- <figure class="tg-authorimg">
                            <img src="/Frontend/img/profile-default-male.png" height="128" width="128" alt="image description"></figure> -->
                            <div class="col-sm-8">
                                         <div class="tg-authorhead">
                                <div class="tg-leftarea" style=" margin-left: -16px;">
                                    <h3>{{$job->title}}</h3>
                                    <span>Job Post: {{App\TimeControl::time_ago($job->created_at)}}  
                                         
                                     </span>
                                      <span style="padding-top: 10px">Job Category: {{$job->job_name}}
                                        
                                     </span>
                                </div>
                                <!-- <div class="tg-rightarea">
                                    <ul class="tg-socialicons tg-socialiconsround">
                                    <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                    <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                    <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                                    <li class="tg-youtube"><a href="javascript:void(0);"><i class="fa fa-youtube"></i></a></li>
                                </ul>
                                </div> -->
                            </div>
                            </div>
                            <div class="tg-description">
                                <p><?php echo ("{$job->description}")?></p>
                            </div>

                        
                     
                           
                       
                          <div class="form-group">
                        <div class="col-sm-2">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="margin-top: 10px" >Apply</button>
                           </div>
                        </div> 
                        </div>
                        
                    </div>
                    </div>
                   @endforeach
                   <div class="box-footer clearfix companypage"><!-- pagination-demo -->
              <ul id="pagination-demo" class="pagination-lg pagination pagination-sm no-margin pull-left">
          
              </ul>
            </div> 
                    </div>
                    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Apply</h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form id="job" name="job">
                        <div class="form-group">
                           <input type="text" name="name" class="form-control" placeholder=" Name" required="true">
                        </div>
                        <div class="form-group">
                        <input type="text"  class="form-control" maxlength="10" name="mobile_number"  placeholder="Enter Mobile Number" required="true" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
                        </div>
                        <div class="form-group">
                           <input type="email" name="email" class="form-control" placeholder="Email"  required="true">
                        </div>
                        <div class="form-group">
                           <input type="text" name="address" class="form-control" placeholder="Address"  required="true">
                        </div>
                        <div class="form-group">
                           <textarea class="form-control" name="description" id="description" placeholder="description" rows="5"  required="true"></textarea>
                        </div>
                        <div class="form-group">
                           <input type="file" class="form-control" name="resume" id="resume"  placeholder="Select Log"   >
                        </div>
                        <div class="form-group">
                        <div class="col-sm-3">
                         <button type="button" name="btnjob" id="btnjob"   class="btn btn-primary btn-block btn-flat" >Send</button>
                           </div>
                        </div>
                     </form>
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary submitBtn" onclick="submitContactForm()">SUBMIT</button> -->
            </div>
      </div>
      
    </div>
  </div>
   <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Apply</h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form id="job1" name="job1">
                        <div class="form-group">
                           <input type="text" name="name1" class="form-control" placeholder=" Name" required="true">
                        </div>
                        <div class="form-group">
                 <input type="text"  class="form-control" maxlength="10" name="mobile_number1"  placeholder="Enter Mobile Number" required="true" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
                        </div>
                        <div class="form-group">
                           <input type="email" name="email1" class="form-control" placeholder="Email"  required="true">
                        </div>
                        <div class="form-group">
                           <input type="text" name="address1" class="form-control" placeholder="Address"  required="true">
                        </div>
                        <div class="form-group">
                           <textarea class="form-control" name="description1" id="description1" placeholder="Request For Job  what type of looking for" rows="5"  required="true"></textarea>
                        </div>
                        <div class="form-group">
                           <input type="file" class="form-control" name="resume1" id="resume1"  placeholder="Select Log"   >
                        </div>
                        <div class="form-group">
                        <div class="col-sm-3">
                         <button type="button" name="btnjob1" id="btnjob1"   class="btn btn-primary btn-block btn-flat" >Send</button>
                           </div>
                        </div>
                     </form>
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary submitBtn" onclick="submitContactForm()">SUBMIT</button> -->
            </div>
      </div>
      
    </div>
   
  </div>

                      @include('Frontend.sidebar.sidebar')
              
           </div>
       </div>
</section>

<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
 <script src="/js/jquery.twbsPagination.js"></script>
<script type="text/javascript">



  // var cat = $('div.company-list').attr('id');
 // alert(cat);

            $('#pagination-demo').twbsPagination({
                totalPages: 2,
                visiblePages: 6,
                onPageClick: function(event, page) {
                 
 // alert("hii");
                      $.job_insert.displayjob(page);
                    // if (!$('page').last().hasClass('active')) {
                    //     var nxt = $('.pagination').find('li.active').find('a').html();
                    //     nxt++;
                    //     $('.pagination').find('li.active').removeClass('active');
                    //     $('.pagination').find('li.page_number#' + nxt).addClass('active');
                      
                    // }
                }
            });
//

     
//    $('#pagination-demo').twbsPagination({

//                 totalPages: 2,
//                 visiblePages: 6,
//                 onPageClick: function(event, page) {

//                        $.complient.display(page);
                    // if (!$('page').last().hasClass('active')) {
                    //     var nxt = $('.pagination').find('li.active').find('a').html();
                    //     nxt++;
                    //     $('.pagination').find('li.active').removeClass('active');
                    //     $('.pagination').find('li.page_number#' + nxt).addClass('active');
                      
                    // }
            //     }
            // });
</script>
    <!-- content end-->
   @endsection
                    