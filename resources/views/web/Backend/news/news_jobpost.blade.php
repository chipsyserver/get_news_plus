@extends('layouts.master_layout1')
@section('content')
        <!-- <header class="panel-heading bg-light">
                                    <ul class="nav nav-tabs nav-justified menu">

 
 <li  ><a href="{{ url('news/international') }}"   >International</a></li>
<li  ><a href="{{ url('news/national') }}">National News</a></li>
<li class="active" ><a href="{{ url('news/state') }}" >State News</a></li>
<li><a href="{{ url('news/local') }}" >Local News</a></li>
                                    </ul>
            
         </header>  -->            


 <div class="news-list" style="padding-top: 10px"  >

 
                                 
                              
                                      
                           
 <section id="up" class="panel panel-default">
                           <header class="panel-heading font-bold text-center ">Job Post</header>


                           <div class="panel-body cat insert"  >
                             
                              <form class="form-horizontal" name="form-insert" >

                               <div class="form-group">
                                <label class="col-sm-2 control-label">Titile</label> 
                                    <div class="col-sm-4"> <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title " required="true"> </div>


                                  </div>
                                   <div class="line line-dashed b-b line-lg pull-in"> </div>
                                   <div class="form-group">
                                    
                                <label class="col-sm-2 control-label">Select State</label>
                                 <div class="col-sm-4">
              <select class="form-control " id="mySelect" name="job_name">
                           <option value='0' selected>Select Job Category</option>
                         @foreach(App\job_category::get() as $job)
                          <!-- $selected = '';
                          if($city->id == 1)    // Any Id
                          {
                              $selected = 'selected="selected"';
                          } -->

                         <option class="job"   value='{{ $job->job_id }}' > {{ $job->job_name}}</option>

                          @endforeach
                          </select>
                          </div>
                          </div>
                         

                                  <!-- <div class="form-group">
                                    <label class="col-sm-2 control-label">Titile</label> 
                                    <div class="col-sm-10"> <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title " required="true"> </div>
                                 </div> -->

                                  <div class="line line-dashed b-b line-lg pull-in"> </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Descriptions</label> 
                                   <div class="col-sm-10"> 
                                   <textarea id="description" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message.." required="true"></textarea>

                                 </div>
                                 </div>


                                               
                      <div class="line line-dashed b-b line-lg pull-in"> </div>
                     

         <div class="row">
         <div class="col-md-1 col-md-offset-2">      

                                     
<button type="button" name="btninsert" id="btninsert"   class="btn btn-primary btn-block btn-flat" style='visibility:show'  >Save</button>
<button type="button" name="btnupdate" id="btnupdate"  class="btn btn-info btn-block btn-flat" value=" " style='visibility:hidden;' >Update</button>
</div>
<div class="col-md-1 ">  
<button type="button" name="btncancel" id="btncancel"   class="btn btn-default btn-block btn-flat"   >Cancel</button>

</div>
</div>
</form>
</div>
</section>
<section id="content">
               <!--    <section class="vbox"> -->
                     <section class="scrollable padder" id="down">
                        <div class="m-b-md">
                           <h3 class="m-b-none">NEWS</h3>
                        </div>
                        <section class="panel panel-default">
                          <!--  <header class="panel-heading"> News <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> </header> -->
                           <div class="table-responsive">
                              <table id ="#page-content " class="table table-striped m-b-none  table-hover" ">
                                 <thead>
                                    <tr align="center">
                                    <th width="5%">ID</th>
                                  
                                    <th width="15%">Title</th>
                                       <th width="10%">Job Type</th>
                                     <th width="25%">Description</th>
                                   
                                    <th width="5%">Action</th>
                                                                           </tr>
                                 </thead>
                                 <tbody class="company-list"> 
                <?php $i=1; ?>
                 @foreach($data as $job)
                 <tr id="{{$job->news_id}}">
                  <td width="5%">{{$i++}}</td>
                  <td width="15%">{{$job->title}}</td>
                  <td width="15%">{{$job->job_name}}</td>

                  <td width="25%">
                                    <?php
$s= strip_tags("  {$job->description}");
echo str_limit($s, $limit = 150, $end = '...');
?></td>
                  
                 
 


<td width="5%"><div class="btn-group ">
                  <button type="button" class="btn btn-default">Action</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu action_control" role="menu">
                    <li id="101"><a href="#" onclick="$('#up').animatescroll();">Edit</a></li>
                    <li id="103"><a href="javascript:void(0)">Delete</a></li>
                 </ul>
                </div></td>
                </tr>
                @endforeach()
                @if ($i==1)  <tr><td colspan="5" style="color:red">No Record Found.</td>
                </tr>
                @endif
                </tbody>
                              </table>
                               <div class="box-footer clearfix companypage">
              <ul id="pagination-demo" class="pagination-lg pagination pagination-sm no-margin pull-left">
          <!--  <li class="prv_page  "><a href="javascript:void(0)" >Previous</a></li>
               <?php $i = 1;  $page=ceil($data->total()/10);?>
                             @for ($i = 1; $i <= $page; $i++)
                               <li  class="page_number @if($i==1)  active @endif" id="{{ $i}}"><a href="javascript:void(0)" >{{ $i}}</a></li>
                             @endfor   
                               <li class="nxt_page"><a href="javascript:void(0)" >Next</a></li>  -->
              </ul>
            </div>
                           </div>
                        </section>
                     </section>
                <!--   </section> -->
                  <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a> 
 <br/><br/>
                  
               </section>

            


                                
                                  


</div>
                  

     



                                      
</section>
       <!-- </div>
      
    </div> -->
    
    @endsection