<!DOCTYPE html>
<html>
 	 @include('header.header')
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
 <section class="vbox">
    <!--  <header class="bg-black header header-md navbar navbar-fixed-top-xs box-shadow panel-heading">

         <div class="navbar-header aside-md dk">
          <a href="index.html" class="navbar-brand">
           <img src="" class="m-r-sm" />
           </a> </div> -->


          
         <header class=" bg-black header header-md navbar navbar-fixed-top-xs box-shadow panel-heading panel-heading ">
        <!--  <div class="navbar-header aside-md dk">
          <a href="index.html" class="navbar-brand">
           <img src="" class="m-r-sm" />
           </a> </div>  -->

                                    <ul class="nav nav-tabs nav-justified menu navbar-static-top">
      <div class="container">
      <div class="navbar-custom-menu">
          <ul class="nav navbar-nav" style="float: left; ">
      
          <li class="active"> <a href="international" class="navbar-brand"  ><b>News Post</b></a>
           <li> <a href="job" class="navbar-brand"  ><b>Job Post</b></a>
        
        </li>
        </ul>
        </div>
       <!-- Collect the nav links, forms, and other content for toggling -->
       
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav" style="float: right; ">
            <li> <a href="/logout" class="navbar-brand"  ><b>Logout</b></a></li>
         <!--  <li><a href="/logout">Logout</a></li> -->
            
           
           
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>



            
         </header>
         </section>

                         

        
     
<div class="wrapper">

  
  <!-- Full Width Column -->
  <div class="content-wrapper">
     <header class="panel-heading bg-light">
                                   <!--  <ul class="nav nav-tabs nav-justified menu">

 
 <li class=" {{ Request::is('news/international') ? 'active' : '' }}" >
 <a href="{{ url('news/international') }}"   ><b>International</b></a></li>
<li class=" {{ Request::is('news/national') ? 'active' : '' }}"  >
<a href="{{ url('news/national') }}"><b>National News</b></a></li>
<li class="{{ Request::is('news/state') ? 'active' : '' }}" >
<a href="{{ url('news/state') }}" ><b>State News</b></a></li>
<li class="{{ Request::is('news/local') ? 'active' : '' }}">
<a href="{{ url('news/local') }}" ><b>Local News</b></a></li>
                                    </ul> -->
            
         </header>   
         <!-- <hr> -->
         
    	 @yield('content')

    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->

</div>


<!-- ./wrapper -->
@include('footer.footer')
 <button type="button" class="load_btns addloading" data-target="body"  style="display:none"> </button>
 <button type="button" class="load_btns loaddiv" data-target="self" style="display:none"></button>
 <button type="button" class="load_btns removeloading" data-target="close" style="position: relative;z-index: 11000;display:none;"></button>
<!-- jQuery 2.2.3 -->
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>


<!-- <script src="//code.jquery.com/jquery-2.1.3.min.js"></script> -->
    <script src="https://raw.githubusercontent.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
    <script src="https://raw.github.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
 

 <script src="/js/animatescroll.js"></script>
<script src="/js/jquery-loader.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.js"></script>

<script src="/alert/js/alert.min.js"></script>

<!-- AdminLTE App -->
<script src="/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

<script src="/js/jquery.twbsPagination.js"></script>
<script src="/js/ckeditor/ckeditor.js"></script>
<script src="/js/news.js"></script>


<!--thems-->

<!---->

<!-- <script src="/js/datepicker/bootstrap-datepicker.js"></script> -->
<!-- slider -->
<!-- <script src="/js/slider/bootstrap-slider.js"></script> -->
<!-- file input -->
<!-- <script src="/js/file-input/bootstrap-filestyle.min.js"></script> -->
<!-- wysiwyg -->
<!-- <script src="/js/wysiwyg/jquery.hotkeys.js"></script>
<script src="/js/wysiwyg/bootstrap-wysiwyg.js"></script>
<script src="/js/wysiwyg/demo.js"></script> -->
<!-- markdown -->
<!-- <script src="/js/markdown/epiceditor.min.js"></script>
<script src="/js/markdown/demo.js"></script>
<script src="/js/chosen/chosen.jquery.min.js"></script>
<script src="/js/spinner/jquery.bootstrap-touchspin.min.js"></script>
<script src="/js/typehead/typeahead.bundle.min.js"></script>
<script src="/js/typehead/demo.js"></script> -->
<!-- <script src="/js/http://pagination.js.org/dist/2.1.0/pagination.js"></script> -->

<!-- script type="text/javascript">
  $(document).ready(function() {
      $(".nav a").on("click", function(){
      $(".nav").find(".active").removeClass("active");
      $(this).parent().addClass("active");
   });
});
</script> -->
<script type="text/javascript">
     CKEDITOR.replace( 'description',
      {
        filebrowserBrowseUrl :'/js/ckeditor/js/ckeditor/filemanager/browser/default/browser.html?Connector=includes/js/editor_files/js/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl : '/js/ckeditor/js/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=includes/js/editor_files/js/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl :'/js/ckeditor/js/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=includes/js/editor_files/js/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl  :'/js/ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl : '/js/ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl : '/js/ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
      });


</script> 
<script>
$(function () {
      $('.load_btns').on('click', function () {   
        $data = {
                autoCheck: $('#autoCheck').is(':checked') ? 32 : false,
                size: $('#size').val(),  
                bgColor: $('#bgColor').val(),  
                bgOpacity: $('#bgOpacity').val(), 
                fontColor: $('#fontColor').val(),  
                title: $('#title').val(),
                isOnly: !$('#isOnly').is(':checked')
            };
      
          switch ($(this).data('target')){
                case 'body':{
                    $.loader.open($data);
                    break;
        }
        case 'self':
                    $('.table-bordered').loader($data);
                    break;
                case 'close':
                    $.loader.close(true);
          break;

            }   
            
        });
});
</script>

</body>
</html>
