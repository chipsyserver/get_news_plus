

$(function() {

    $.mycity = {




        init: function(callback) {


                $("[name=btnsubmit]").click(function(){
                                
               $.mycity.sendmessage();
            });

                    $("[name=addcity]").click(function(){
                                
               $.mycity.addcity();
            });


                $("[name=btncancel]").click(function(){
                                 $('li.parsley-required').remove();
                                $('form[name=mycity]').get(0).reset();
                                $('[name=mycity]').parsley().reset();
                                CKEDITOR.instances.description.setData(' ');

                              $('#btnsubmit').attr('style', 'visibility:show');
                    $('#btnupdate').attr('style', 'visibility:hidden;margin-top: -35px;');
                      $("div.img").show();
         $('#show').attr('style', 'visibility:hidden;border: 0px; background: none;');
        
            });

            
            $("[name=form-insert] input").keyup(function(e) {

                $.mycity.validate($(this), callback);
            });

            $("[name=form-insert] div").keyup(function(e) {
                $.mycity.validate($(this), callback);
            });

            $(document).on('click', '.company-list ul.action_control > li', function() {
                 
                $.mycity.action_controls($(this));
            });
            $(document).on('click', '[name=btnupdate]', function() {

               
                $.mycity.update($(this));

            });

        },




        validate: function(ths) {
            // check required fileds
            $('.error-report').html('');
            if (ths.val() != "") {
                ths.css('background-color', 'white').css('border-color', ' ');
            }


        },
  action_controls: function(ths) {
            var action_id = ths.attr('id');
            // alert(action_id);
       
            switch (parseInt(action_id)) {
                case 103:
                    $.mycity.delete($(ths));
                    break
                case 101:
                 $('#up').animatescroll();
                    $.mycity.edit($(ths));
                    break
            }
        },




         update: function(ths) {
            var id = ths.val();
          
              var proceed =true;
           if (proceed) {  
         
               $.confirm({
                             title: 'confirm',
                         content: "Do you Want update",
                        type: 'blue',
                       typeAnimated: true,
                          buttons: {
                          tryAgain: {
                         text: 'ok',
                        btnClass: 'btn-blue',
                   action: function(){
                

           
                      var postdata = new FormData();
                         

                 
                         
                            postdata.append('name', $('#name').val());
                            postdata.append('phone', $('#phone').val());
                            postdata.append('landline',$('#landline').val());
                            postdata.append('email', $('#email').val());
                            // postdata.append('address', $('#address').val());
                       
                               var value = CKEDITOR.instances['description'].getData();
                postdata.append('address', value);
                            postdata.append('city_id', $('[name=city').val());
                            postdata.append('category_id', $('[name="category').val());
                           var file = document.getElementById('product_image').files[0];
                        if (file) {
                     postdata.append('product_image', file);
                 }

         

$(".addloading").click();
          
            $.ajax({
                url: "updatecity/" + id,
                async: !1,
                type: "POST",
                dataType: "json",
                contentType: false,
                cache: false,
                processData: false,
                data: postdata,
                success: function(response) {
                    $(".removeloading").click();
                    if (response.success) {

                        var display = response.message;
                           
                             $.confirm({
                                        title: 'Congratulations',
                                        content: display,
                                        type: 'green',
                                        typeAnimated: true,
                                        buttons: {
                                            tryAgain: {
                                                text: 'ok',
                                                btnClass: 'btn-green',
                                                action: function(){
                                                    $('#down').animatescroll();
                                                       setTimeout(function(){
                                                   $.mycity.display(1);
                                                     },1000)


                                                        $('form[name=mycity]').get(0).reset();
                    $('#btnsubmit').attr('style', 'visibility:show');
                    $('#btnupdate').attr('style', 'visibility:hidden;margin-top: -35px;');
                      $("div.img").show();
         $('#show').attr('style', 'visibility:hidden;border: 0px; background: none;');
         CKEDITOR.instances.description.setData(' ');
                                                }
                                            },
                                            // close: function () {
                                            // }
                                        }
                                    });
                           
                     
                        
                       
                    } else {
                        var display = "";
                        if ((typeof response.message) == 'object') {
                            $.each(response.message, function(key, value) {
                                display = key.replace("_", " ").toUpperCase() + ' ' + value[0];
                            });
                        } else {
                            display = response.message;
                        }

                        $.confirm({
                        title: 'Encountered an error!',
                        content: display,
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Error',
                                btnClass: 'btn-red',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                  }

              }
               });
             }
                 },

                                            close: function () {
                                            }
                                        
                                   
            // },500);
            //         }
                }
            
            });
            }
        },



        delete: function(ths) {

            var id = ths.closest('tr').attr('id');
               $.confirm({
                        title: 'Delete',
                        content: "Are you Sure Want to Delete",
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Ok',
                                btnClass: 'btn-red',
                                action: function(){
 $(".addloading").click();
             
            $.ajax({
                url: 'deletemycity/' + id,
                type: 'DELETE',
                success: function(response) {
                    $(".removeloading").click();
                    if (response.success) {
                        $('tbody.company-list tr#' + id).css('background-color', 'rgb(181, 117, 117)').hide(1000);
                        setTimeout(function() {
                            if ($('tbody.company-list tr:visible').length == 0) {
                                var display = "<tr><th colspan=4 style='color:red'>No Records Found </th></tr>";
                                $('tbody.company-list').html(display);
                            }
                        }, 1001);
                        display=response.message;

                     
                          //$.news.display(1);

                    } else {
                        $.alert.open('error', 'Something went wrong');
                    }

                }
            });

             }
                            },
                            close: function () {
                            }
                        }
                    });



        },
        
          edit: function(ths) {


            var id = ths.closest('tr').attr('id');
          


            $.ajax({
                url: 'edit_mycity/' + id,
                type: "GET",
                dataType: "json",

                success: function(response) {
                   
                   
                      $('#name').val(response[0].name);
                      $('#phone').val(response[0].mobile);
                      $('#landline').val(response[0].land_line) ;
                      $('#email').val(response[0].email);
                       CKEDITOR.instances.description.setData(response[0].address);
                      // $('#address').val(response[0].address);
                      $('[name=city').val(response[0].city_id);
                      $('[name="category').val(response[0].shop_id);

                    $('#btnsubmit').attr('style', 'visibility:hidden');
                    $('#btnupdate').attr('style', 'visibility:show; margin-top: -34px;');
                     $('#btncancel').attr('style', 'visibility:show;');
                    $('#btnupdate').attr('value',response[0].mycity_id);
                    $("div.img").hide();
                    $('#show').attr('style', 'visibility:show;border: 0px; background: none;margin-top:15px');
                    document.getElementById("show").value = "submit";
                    $("#show").click(function() {
                        $("div.img").show();
                    });




                    // $("#title").html(data);


                },
                error: function() { /* error code goes here*/ }
            });



        },


        
          pagination: function() 
        {

         


        },

        display: function(offset) {



            //display companys
          $(".addloading").click();
            $.get('getdisplaymycity?page=' + offset, function(response) {
        $(".removeloading").click();
                var display = "";
            
                // alert(response.data[0].name);
                // alert(JSON.stringify(result.json));
                
                // alert('products->total()');
               

                for (var i = 0; i < response.data.length; i++) {
                   
                     
                      var regex = /(<([^>]+)>)/ig
                    var body = response.data[i].address;
                    var string = body.replace(regex, "");

                    //alert(result);
                                        var dots = "...";
                                         if(string.length > 150)
                      {
                        // you can also use substr instead of substring
                        string = string.substring(0,150) + dots;
                      }

    

                    display += '<tr id=' + response.data[i].mycity_id + '><td width="5%">' + parseInt(parseInt(i + 1) + parseInt((offset - 1) * 5)) +  '</td><td width="10%"><img width="100" src="/img'+ response.data[i].category + '/'  + response.data[i].year + '/' + response.data[i].month + '/' + response.data[i].image_name + '_l.' +response.data[i].ext + '"> </td><td width="5%">' + response.data[i].name + '</td><td width="5%">' +response.data[i].mobile +'</td><td width="5%">'+response.data[i].email +'</td><td width="5%">'+string  +'</td><td width="5%">'+response.data[i].city+'</td><td width="5%">'+response.data[i].shop_category+'</td><td width="10%"><div class="btn-group "><button type="button" class="btn btn-default">Action</button> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span></button> <ul class="dropdown-menu action_control" role="menu"> <li id="101"><a href="javascript:void(0)" class="up">Edit</a></li><li id="103"><a href="javascript:void(0)">Delete</a></li></ul></div></td></tr>';
                    //alert(parseInt(parseInt(i + 1) + parseInt((offset - 1) * 5)));
                }

                display = display == "" ? '<tr><th colspan=4 style="color:red">No Records Found </th></tr>' : display;

                // alert(display);
                $('.company-list').html(display);

            }, 'json');
        },

        

 addcity:function()
        {
            alert("hii");
            var proceed = $(' [name=city]').parsley().validate(); 
           if (proceed) {
            var postdata = new FormData();
            postdata.append('city', $('#city').val());
            alert($('#city').val());
            $(".addloading").click();
 
                 $.ajax({
                    url: "add_city",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {
                    $(".removeloading").click();
                   
                    if (response.success) {



                            var display = response.message;
                             $.confirm({
                                        title: 'Congratulations',
                                        content: display,
                                        type: 'green',
                                        typeAnimated: true,
                                        buttons: {
                                            tryAgain: {
                                                text: 'ok',
                                                btnClass: 'btn-green',
                                                action: function(){



                                                    
                                        

                                                }
                                            },
                                            close: function () {
                                            }
                                        }
                                    });
                           
                      
                             $('[name=city]').get(0).reset();


                 

                        } 
                    }
                    
                });
                 $(".removeloading").click();


                         }
            },



        sendmessage: function(ths, callback) {
           
            var proceed = false;
            var form = $(' [name=mycity]');
        var proceed = $(' [name=mycity]').parsley().validate(); 
       
            if (proceed) {

               
                    var postdata = new FormData();
                         

                 
                         
                            postdata.append('name', $('#name').val());
                            postdata.append('phone', $('#phone').val());
                            postdata.append('landline',$('#landline').val());
                            postdata.append('email', $('#email').val());
                          
                             var value = CKEDITOR.instances['description'].getData();
                              postdata.append('address', value);
                            postdata.append('city_id', $('[name=city').val());
                            postdata.append('category_id', $('[name="category').val());
                           var file = document.getElementById('product_image').files[0];
                        if (file) {
                     postdata.append('product_image', file);
                 }

               
 $(".addloading").click();
 
                 $.ajax({
                    url: "mycity",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {

 $(".removeloading").click();


                        if (response.success) {



                            var display = response.message;
                             $.confirm({
                                        title: 'Congratulations',
                                        content: display,
                                        type: 'green',
                                        typeAnimated: true,
                                        buttons: {
                                            tryAgain: {
                                                text: 'ok',
                                                btnClass: 'btn-green',
                                                action: function(){



                                                    $('#down').animatescroll();
                                                       setTimeout(function(){
                                                    $.mycity.display(1);
                                                     },1000);

                                                }
                                            },
                                            close: function () {
                                            }
                                        }
                                    });
                           
                      
                             $('form[name=mycity]').get(0).reset();

                             CKEDITOR.instances.description.setData(' ');
                 

                        } else {
                            var display = "";
                            if ((typeof response.message) == 'object') {
                                $.each(response.message, function(key, value) {
                                    display = value[0]; //fetch oth(first) error.

                                });
                            } else {
                                // alert(JSON.stringify(response));


                                display = response.message;
                            }
                             $.confirm({
                            title: 'Encountered an error!',

                            content: display,
                            type: 'red',

                            typeAnimated: true,
                            buttons: {
                                
                                close: function () {
                                }
                            }
                        });

                        }
                    }
                });


            }


        },








            

          }
    $.mycity.init();
    $.mycity.pagination();
   
}(jQuery));