$(function(){  

	$.login = {
		init: function(callback) {
			$("[name=login_from] [name=btnsubmit]").click(function() { 	$.login.login($(this),callback); });
			$("[name=login_from] input").keyup(function(e) { var code = (e.keyCode ? e.keyCode : e.which);
			if (code == 13) { $.login.login(); } $.login.validate($(this),callback); });
		},
		validate:function(ths,callback){ 
			if(ths.val()!=""){ ths.css('border-color','');	}
			$('.message_area').html('');	
			 			
		},
		login: function(ths,callback) {
		    var proceed = true;
			$('[name=login_from] [required=true]').filter(function() {
				if ($.trim(this.value) == "") {
					$(this).css('border-color','red');
					proceed = false;
				}
            }); 
			
            if(proceed) { 
				var postdata={}
				var input = $('[name=login_from]').serializeArray();
				$.each(input, function() {
					if (postdata[this.name] !== undefined) {
						if (!postdata[this.name].push) {
								postdata[this.name] = [o[this.name]];
						}
						postdata[this.name].push(this.value || '');
					} else {
						postdata[this.name] = this.value || '';
					}
					
				 });
				$.post('superadmin/auth/postlogin',postdata,function(response){

					//alert(JSON.stringify(response));
			 	   if(response.success){
			 	    var display = response.message;
			 	   	  
			 	// $.confirm({
     //                                    title: 'Congratulations',
     //                                    content: display,
     //                                    type: 'green',
     //                                    typeAnimated: true,
                                      
     //                                });
			 	   window.location="superadmin/news/international"; 
						
					}else {
						  // display the errror message
						        var display = "";
								 if ((typeof  response.message) == 'object') { 
								$.each( response.message, function( key, value ) {
									display =value[0] ; //fetch oth(first) error.
								}); }else{  
                                display = response.message ;               
								}
								$('.message_area').html(display).css('color','red');
				                $('[name=login_from] input').val('');
						
					}	
				},'json');
		  }
		},
		
	}
	$.login.init();
})(jQuery);