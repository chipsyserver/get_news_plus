
$(function() {

    $.news = {


        init: function(callback) {


                $("[name=btninsert]").click(function(){
                                
               $.news.sendmessage();
            });


                $("[name=btncancel]").click(function(){
                                
               CKEDITOR.instances.description.setData(' ');
                             $('form[name=form-insert]').get(0).reset();
                              $('#btninsert').attr('style', 'visibility:show');
                    $('#btnupdate').attr('style', 'visibility:hidden;margin-top: -35px;');
                      $("div.img").show();
         $('#show').attr('style', 'visibility:hidden;border: 0px; background: none;');
            });

            
            $("[name=form-insert] input").keyup(function(e) {

                $.news.validate($(this), callback);
            });

            $("[name=form-insert] div").keyup(function(e) {
                $.news.validate($(this), callback);
            });

            $(document).on('click', 'ul.action_control > li', function() {
            
                 
                $.news.action_controls($(this));
            });
            $(document).on('click', '.news-list [name=btnupdate]', function() {

               
                $.news.update($(this));

            });

        },

        

        action_controls: function(ths) {
            var action_id = ths.attr('id');

              switch (parseInt(action_id)) {
                case 103:
                    $.news.delete($(ths));
                    break
                case 101:
                 $('#up').animatescroll();
                    $.news.edit($(ths));
                    break
            }
        },


        validate: function(ths) {
            // check required fileds
            $('.error-report').html('');
            if (ths.val() != "") {
                ths.css('background-color', 'white').css('border-color', ' ');
            }


        },

        update: function(ths) {
            var id = ths.val();
            var proceed = true;

            // $('[name=news] [required=true]').filter(function() {
            //  if ($.trim(this.value) == "") {
            //      $(this).css('border-color', 'red');
            //      proceed = false;
            //  }
            $('[name=from_company] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
           if (proceed) {  

         
            $.confirm({
            title: 'Update',
            content: 'Are sure want to update ..!!',
            type: 'blue',
            typeAnimated: true,
            buttons: {
            tryAgain: {
            text: 'ok',
            btnClass: 'btn-blue',
            action: function() {
            var postdata = new FormData();
            postdata.append('title', $('[name=title]').val());

              postdata.append('state_id', $('[name=city]').val());
                var priority= $("[name=priority]:checked").val();
                postdata.append('priority', priority);

                var cat = $('div.cat').attr('id');
                postdata.append('cat', cat);

                var value = CKEDITOR.instances['description'].getData();
                postdata.append('description', value);

                var file = document.getElementById('product_image').files[0];
                if (file) {
                postdata.append('product_image', file);
                   }


            // $(".addloading").click();
            /*setTimeout(function(){*/
            $.ajax({
                url: "update/" + id,
                async: !1,
                type: "POST",
                dataType: "json",
                contentType: false,
                cache: false,
                processData: false,
                data: postdata,
                success: function(response) {
                    $(".removeloading").click();
                    if (response.success) {

                        var display = response.message;
                           
                             $.confirm({
                                        title: 'Congratulations',
                                        content: display,
                                        type: 'green',
                                        typeAnimated: true,
                                        buttons: {
                                            tryAgain: {
                                                text: 'ok',
                                                btnClass: 'btn-green',
                                                action: function(){
                                                    $('#down').animatescroll();
                                                       setTimeout(function(){
                                                    $.news.display(1);
                                                     },1000)
                                                }
                                            },
                                            // close: function () {
                                            // }
                                        }
                                    });
                           
                       CKEDITOR.instances.description.setData(' ');
                             $('form[name=form-insert]').get(0).reset();
                              $('#btninsert').attr('style', 'visibility:show');
                    $('#btnupdate').attr('style', 'visibility:hidden;margin-top: -35px;');
                      $("div.img").show();
         $('#show').attr('style', 'visibility:hidden;border: 0px; background: none;');
                   
                            // $.news.display(1);
                        
                       
                    } else {
                        var display = "";
                        if ((typeof response.message) == 'object') {
                            $.each(response.message, function(key, value) {
                                display = key.replace("_", " ").toUpperCase() + ' ' + value[0];
                            });
                        } else {
                            display = response.message;
                        }

                        $.confirm({
                        title: 'Encountered an error!',
                        content: display,
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Error',
                                btnClass: 'btn-red',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });

                    }
                }
            });
            }
                                            },
                                            close: function () {
                                            }
                                        }
                                    });
            }
        },



        
          pagination: function() 
        {

         


        },

        display: function(offset) {


           
            //display companys
            $(".addloading").click();
            $.get('getdisplay?page=' + offset, function(response) {
                $(".removeloading").click();
                var display = "";
                
                // alert('products->total()');
               

                for (var i = 0; i < response.data.length; i++) {
                   

                      var regex = /(<([^>]+)>)/ig
                    var body = response.data[i].description;
                    var string = body.replace(regex, "");

                    //alert(result);
                                        var dots = "...";
                                         if(string.length > 150)
                      {
                        // you can also use substr instead of substring
                        string = string.substring(0,150) + dots;
                      }


    

                    display += '<tr id=' + response.data[i].news_id + '><td width="5%">' + parseInt(parseInt(i + 1) + parseInt((offset - 1) * 10)) +  '</td><td width="10%"><img width="100" src="/img'+ response.data[i].category + '/'  + response.data[i].year + '/' + response.data[i].month + '/' + response.data[i].image_name + '_l.' +response.data[i].ext + '"> </td><td width="10%">' + response.data[i].title + '</td><td width="15%">' + string + '</td><td width="10%"><div class="btn-group "><button type="button" class="btn btn-default">Action</button> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span></button> <ul class="dropdown-menu action_control" role="menu"> <li id="101"><a href="javascript:void(0)" class="up">Edit</a></li><li id="103"><a href="javascript:void(0)">Delete</a></li></ul></div></td></tr>';
                    //alert(parseInt(parseInt(i + 1) + parseInt((offset - 1) * 5)));
                }

                display = display == "" ? '<tr><th colspan=4 style="color:red">No Records Found </th></tr>' : display;

                // alert(display);
                $('.company-list').html(display);

            }, 'json');
        },



   


        edit: function(ths) {


            var id = ths.closest('tr').attr('id');
         


            $.ajax({
                url: 'edit' + id,
                type: "GET",
                dataType: "json",

                success: function(response) {
                    //alert(JSON.stringify(response));
                    data1 = response.data1;
                    data2 = response.data2;
                    data3 = response.data3;
                    data4 = response.data4;
                    data5 = response.data5;
                    
                    if(data5==1)
                    {
                      document.getElementById("priority").checked = true;
                    }
                    
                    $('#title').val(data2);
                    CKEDITOR.instances.description.setData(data3);
                   
                      $('#mySelect').val(data4);
                    $('#btninsert').attr('style', 'visibility:hidden');
                    $('#btnupdate').attr('style', 'visibility:show;margin-top: -34px;');
                     $('#btncancel').attr('style', 'visibility:show;');
                    $('#btnupdate').attr('value', data1);
                    $("div.img").hide();
                    $('#show').attr('style', 'visibility:show;border: 0px; background: none;');
                    document.getElementById("show").value = "submit";
                    $("#show").click(function() {
                        $("div.img").show();
                    });




                  


                },
                error: function() { /* error code goes here*/ }
            });



        },



        delete: function(ths) {

            var id = ths.closest('tr').attr('id');
             $.confirm({
                        title: 'Delete',
                        content: 'Are u sure Want to Delete!!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Ok',
                                btnClass: 'btn-red',
                                action: function(){
            $.ajax({
                url: 'delete/' + id,
                type: 'DELETE',
                success: function(response) {
                    $(".removeloading").click();
                    if (response.success) {
                        $('tbody.company-list tr#' + id).css('background-color', 'rgb(181, 117, 117)').hide(1000);
                        setTimeout(function() {
                            if ($('tbody.company-list tr:visible').length == 0) {
                                var display = "<tr><th colspan=4 style='color:red'>No Records Found </th></tr>";
                                $('tbody.company-list').html(display);
                            }
                        }, 1001);
                        display=response.message;

                        $.confirm({
                        title: 'Delete',
                        content: display,
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Ok',
                                btnClass: 'btn-red',
                                action: function(){

                                                       setTimeout(function(){
                                                    // $.news.display(1);
                                                     },1000)
                                }
                            },
                            // close: function () {
                            // }
                        }
                    });


                          //$.news.display(1);

                    } else {
                        $.alert.open('error', 'Something went wrong');
                    }

                }
            });

 }
                            },
                            close: function () {
                            }
                        }
                    });


        },




        sendmessage: function(ths, callback) {

            var proceed = true;
            $('.tab-pane.active [name=news] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                } 

                else {
                    var extension = $('#product_image').val().split('.').pop().toLowerCase();


                    if ($.inArray(extension, ['png', 'jpg']) == -1) {
                        $.alert({
                            title: 'We only use .jpg and png only',
                            content: 'file extension is error',
                        });
                        proceed = false;


                    }
                }

            });




            if (proceed) {

                var postdata = new FormData();
               
               
                var priority= $("[name=priority]:checked").val();
                postdata.append('priority', priority);
                postdata.append('title', $('[name=title]').val());
                  postdata.append('state_id', $('[name=city]').val());


                var cat = $('div.cat').attr('id');
                postdata.append('cat', cat);

                
            
  //var strChosen = $('option').attr('value');
 //var state = $('option.state').attr('value');
       // alert($('[name=city]').val());    

                var value = CKEDITOR.instances['description'].getData();
                postdata.append('description', value);

                var file = document.getElementById('product_image').files[0];
                if (file) {
                    postdata.append('product_image', file);
                }




                //var postdata = new FormData();




                // postdata.append("product_image", $('input[name=product_image]')[0].files[0]);

                // var form = new FormData();   
                //    form.append("import_file",$("#import")[0].files[0]);




                $.ajax({
                    url: "store",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {




                        if (response.success) {



                            var display = response.message;
                             $.confirm({
                                        title: 'Congratulations',
                                        content: display,
                                        type: 'green',
                                        typeAnimated: true,
                                        buttons: {
                                            tryAgain: {
                                                text: 'ok',
                                                btnClass: 'btn-green',
                                                action: function(){



                                                    $('#down').animatescroll();
                                                       setTimeout(function(){
                                                    $.news.display(1);
                                                     },1000);

                                                }
                                            },
                                            close: function () {
                                            }
                                        }
                                    });
                           
                       CKEDITOR.instances.description.setData(' ');
                             $('form[name=form-insert]').get(0).reset();

$.news.display(1);
                       // display += '<tr id=' + response.data[i].news_id + '><td>' + parseInt(parseInt(i + 1) + parseInt((offset - 1) * 5)) +  '</td><td><img width="200" src="/img'+ response.data[i].category + '/'  + response.data[i].year + '/' + response.data[i].month + '/' + response.data[i].image_name + '_l.' +response.data[i].ext + '"> </td><td>' + response.data[i].title + '</td><td><?php echo strip_tags(' + response.data[i].description + '</td><td><div class="btn-group "><button type="button" class="btn btn-default">Action</button> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span></button> <ul class="dropdown-menu action_control" role="menu"> <li id="101"><a href="#up">Edit</a></li><li id="103"><a href="javascript:void(0)">Delete</a></li></ul></div></td></tr>';

                            //   $('.message_area1').html(display).css('color', 'green');
                            //  data1=response.data1;
                            //  $('#value1').html(data1).css('color','blue');
                            // window.location="/company/create";

                        } else {
                            var display = "";
                            if ((typeof response.message) == 'object') {
                                $.each(response.message, function(key, value) {
                                    display = value[0]; //fetch oth(first) error.

                                });
                            } else {
                                // alert(JSON.stringify(response));


                                display = response.message;
                            }
                             $.confirm({
                            title: 'Encountered an error!',

                            content: display,
                            type: 'red',

                            typeAnimated: true,
                            buttons: {
                                
                                close: function () {
                                }
                            }
                        });
                             // CKEDITOR.instances.description.setData(' ');
                             // $('form[name=form-insert]').get(0).reset();



                            // $('.position').html(position).css('color', 'red');

                            // $('.number').html(number).css('color', 'red');
                            // $('.message_area1').html(display).css('color', 'red');

                            //   $('[name=form-insert] textarea').val('');


                        }
                    }
                });


            }


        },




    }
    $.news.init();
    $.news.pagination();
}(jQuery));