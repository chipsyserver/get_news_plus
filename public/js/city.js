var myApp = angular.module('news', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("NewsController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
     

   $scope.isDisabled = true;

           
    $scope.showSave = false;
$scope.showBtns = true;




$scope.cancel = function(){
  $scope.getVisitors(1); 
}



$scope.edit = function(){
   var id=angular.element(event.currentTarget).attr('id');
   // alert(id);
    document.getElementById(id).disabled = false;
     $('#'+id).focus().select();

 $scope.showSave=true;
}

$scope.delete = function(event){
       var id=angular.element(event.currentTarget).attr('id');
      

       $.confirm({    title: 'Delete',
                        content: "Are you Sure Want to Delete",
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Ok',
                                btnClass: 'btn-red',
                                action: function(){
 $(".addloading").click();
             
            $.ajax({
                url: 'city/' + id,
                type: 'DELETE',
                success: function(response) {
                    $(".removeloading").click();
                    if (response.success) {
                        $('tbody.company-list tr#' + id).css('background-color', 'rgb(181, 117, 117)').hide(1000);
                        setTimeout(function() {
                            if ($('tbody.company-list tr:visible').length == 0) {
                                var display = "<tr><th colspan=4 style='color:red'>No Records Found </th></tr>";
                                $('tbody.company-list').html(display);
                            }

                            $scope.getVisitors(1); 

                        }, 1001);
                       

                     
                          //$.news.display(1);

                    } else {
                        $.alert.open('error', 'Something went wrong');
                    }

                }
            });

             }
                            },
                            close: function () {
                            }
                        }
                    });
          
}


 $scope.update = function(event) {
                                
          
           var id=angular.element(event.currentTarget).attr('id');
          
            
      // alert(id);
        
        var proceed = true

        if (proceed) {
             $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to Update details?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Yes',
                        btnClass: 'btn-orange',
                        action: function() {

            var postdata = new FormData();
            postdata.append('city', $('#'+id).val());
                    
                $(".addloading").click();
 
                $.ajax({
                    url: "update_city/"+id,
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {
                      if (response.success) {
                                  $(".removeloading").click();
                                   var display = response.message;
                                    $.confirm({
                                        title: 'Congratulations',
                                        content: display,
                                        type: 'green',
                                        typeAnimated: true,
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                 btnClass: 'btn-green',
                                                action: function() {
                                                    // ('user_profile').reset();
                                                 $scope.showSave = false;
                                                  $scope.showBtns = true;
                                                       $scope.isDisabled = false;
                                                      $("#"+response.id).prop('disabled', true);
                
                                                 
                                                 $scope.getVisitors(1);  

                                              }
                                            },
                                            close: function () {
                                            },
                                        }
                                    });
                                }
                                 else {
                                    var display = "";
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display = value[0];
                                        });
                                    } else {
                                        display = response.message;
                                    }
                                    form.find('.resp-msg').html('* '+display).css('color', 'red');
                                }
                              }
                           });
                        }
                                    },
                                    cancel: function() {}
                                }
                            });
                        
        }
    }
  $("[name=addcity]").click(function(){
                               
              
                var proceed =true; 
           if (proceed) {
            var postdata = new FormData();
            postdata.append('city', $('#city').val());
           
            $(".addloading").click();
 
                 $.ajax({
                    url: "add_city",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {
                    $(".removeloading").click();
                   
                    if (response.success) {



                            var display = response.message;
                             $.confirm({
                                        title: 'Congratulations',
                                        content: display,
                                        type: 'green',
                                        typeAnimated: true,
                                        buttons: {
                                            tryAgain: {
                                                text: 'ok',
                                                btnClass: 'btn-green',
                                                action: function(){



                                                 $scope.getVisitors(1);   
                                        

                                                }
                                            },
                                            close: function () {
                                            }
                                        }
                                    });
                           
                      $('#city').val(' ');
                             $('[name=city]').get(0).reset();


                 

                        } 
                    }
                    
                });
                 $(".removeloading").click();


             }
            });

    // Pagination of Visitors
    $scope.maxSize = 3;
 



       
        // var data="pending";
       


    //load Visitors     
    $scope.getVisitors = function(page=1) {
      $scope.isDisabled = true;
      $scope.showSave = false;
      $scope.showBtns = true;
       $http.get('displaycity?page=' + page)
            .then(function(res) {
                
                $scope.visitors = res.data.data;

                $scope.totalVisitors = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.visitorsPerPage = res.data.per_page;
                $scope.visitorsFrom = res.data.from ? res.data.from : 0;
                $scope.visitorsTo = res.data.to ? res.data.to : 0;

                
                $scope.displaybills = res.data.data;
                  $scope.page = (page-1)*res.data.per_page;
                  $('[name=pagination]').twbsPagination({
              totalPages: res.data.last_page,
              visiblePages: 7,
              onPageClick: function (event, page) {
              $scope.loadinfo(page);
              }
           }); 
        });
    }
 $scope.getVisitors(1);
   
}]);

