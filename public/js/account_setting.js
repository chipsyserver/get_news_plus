$(function() {
    $.setting = {
        control: function() {
            //add fileds for updating password
            $("[name=change_password]").click(function() {
                $.setting.add_password($(this));
            });
            $(' [required=true]').on('keyup change', function() {
                $('.display_message').html('');
                $(this).css('border-color', '');
            }); // cahnge background color 
            $("[name=submitaccount]").click(function() {
               

                $.setting.update_account($(this));
            });

        },
        add_password: function(ths) {
            if (ths.prop('checked')) {
                var diplay = '<div class="form-group updatepassword"><label for="email" class="control-label col-sm-2 right">New Password</label>' +
                    '<div class="col-sm-10"><input type="password" name="new_password" class="form-control pass_control" required="true"></div></div><div class="col-md-12 updatepassword">&nbsp;</div> <div class="form-group updatepassword">' +
                    '<label for="email" class="control-label col-sm-2 right">Retype Password</label><div class="col-sm-10">' +
                    '<input type="password" name="re_password" class="form-control pass_control" required="true" ></div></div><div class="col-md-12 updatepassword">&nbsp;</div> ';
                $('.check_changepassword').after(diplay);
            } else {
                $('.updatepassword').remove();
            }
        },
        //update account
        update_account: function(ths) {
            var proceed = true;
            $('[name=account_fromdata] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    //checks  for null value if trues add color to highlight border-color
                    $(this).css('border-color', '#a94442');
                    proceed = false;
                    $("html, body").animate({
                        scrollTop: 0
                    }, 600);
                }
            });
            if (proceed) {
                // $.alert.open({
                //     type: 'confirm',
                //     content: 'Are sure want to upadate account ..!!',
                //     callback: function(button) {
                //         if (button == 'yes') {
                            var postdata = {}
                            var input = $('[name=account_fromdata]').serializeArray();
                            $.each(input, function() {
                                if (postdata[this.name] !== undefined) {
                                    if (!postdata[this.name].push) {
                                        postdata[this.name] = [o[this.name]];
                                    }
                                    postdata[this.name].push(this.value || '');

                                } else {
                                    postdata[this.name] = this.value || '';
                                }
                            });
                            $(".addloading").click();
                            $.post('update_account', postdata, function(response) {
                                $(".removeloading").click();
                                if (response.success) {
                                    $(".form-control").not('[name=uname]').val('');
                                    var display = "";
                                    display = response.message; //showing only the first error.
                                    $('.display_message').html(display).css('color', 'green');
                                    $('.updatepassword').remove();
                                    $('input:checkbox').removeAttr('checked');
                                } else {
                                    var display = "";
                                    $('.pass_control').val('');
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display += value[0]; //showing only the first error.
                                        });
                                    } else {
                                        display += response.message; //showing only the first error.
                                    }
                                    $('.display_message').html(display).css('color', 'red');
                                }
                            }, 'json');
                //         }
                //     }
                // });
            }
        }
    }
    $.setting.control();
})(jQuery);