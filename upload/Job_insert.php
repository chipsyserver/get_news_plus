<?php 
namespace App\Http\Controllers\Web\Frontend;
use App\Http\Controllers\Controller;
use  DB;
use Hash;
use Illuminate\Http\Request;
use Validator;
use File;
use Mail;
use App\Repository\Backend\DropdwonControl;
class Job_insert extends Controller {
   
    public function job_insert(Request $request){ 
	
		    $rules = array( 'name'=> 'required','mobile_number' => 'required','email'=>'required','address'=>'required','message'=>'required','resume' => 'required|mimes:doc,docx,odt|max:2048');
	    $validator = Validator::make($request->all(), $rules); 
		$data = $request->all();
	
	    if ($validator->fails()) {
			return response()->json(array(
					'success' => false,
					'message' => $validator->getMessageBag()->toArray()
					));
	   }  
		// $timestmp=time();
		if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
		     return response()->json(array(
		            	'success' => false,
						'message' => "Invalid Email address."
						));
			}

		    $mob="/^[6789][0-9]{9}$/";
			if(!preg_match($mob,$data['mobile_number']))
			{	  return response()->json(array(
		            	'success' => false,
						'message' => "Invalid Mobile number."
						));
			}


	
		  // DB::table('count')->increment('presentcount');
		  // return response()->json(array(
		  //           	'success' => true,
				// 		'message' => "message is finished."
				// 		));



		


	
// die(json_encode($count[0]->presentcount));
		   // $usercont = DB::table('count')->where($count[0]->presentcount,$count[0]->presentcount)->count();

	

		
		// else{
			$name= $request->get('name');
                 $mobile_number= $request->get('mobile_number');
                        
                $email =$request->get('email');
               $address =$request->get('address');
				 $message =$request->get('message');
				 // echo $message;
		 	  $insert[] = ['name'=>$name,'mobile'=>$mobile_number,'email' =>$email,'address'=>$address,'message'=>$message];
		     DB::table('profile')->insert($insert);




		      $data['profile_id']=DB::getPdo()->lastInsertId();


		 $file = $request->file('resume');

       $allowed =  array('doc', 'docx','odt');
        $filename = $_FILES['resume']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) {
           return response()->json(array(
                                'success' => false,
                                'message' => "Extension error only  doc/docx  or odt  valid "
                        ));
                }
       
       
        if($file!=null) {   
        $file_cat="file"; 

        $i_year=date('Y'); $i_month=date('m'); 
        $file_name=uniqid();
         $ext=$file->getClientOriginalExtension(); 
        if (!is_dir($file_cat)) {
            	mkdir($file_cat);
        }
        
        if (!is_dir($file_cat . "/" . $i_year)) {
            mkdir($file_cat . "/" . $i_year);
        }
        if (!is_dir($file_cat . "/" . $i_year . "/" . $i_month)) {
            mkdir($file_cat . "/" . $i_year . "/" . $i_month);
        }
 

      

      $file->move($file_cat.'/'.$i_year.'/'.$i_month.'/', $file_name.'.'.$ext  );


  

 }
 $file=DB::insert('insert into file(profile_id,category,year,month,file_name,ext) values(?,?,?,?,?,?)',array($data['profile_id'],$file_cat,$i_year,$i_month,$file_name,$ext));

             







		   return response()->json(array(
		            	'success' => true,
		            	'message' => "Your profile Send Successfully "
		 				));
		 }



		 public function job_insert1(Request $request){ 
	
		    $rules = array( 'name'=> 'required','mobile_number' => 'required','email'=>'required','address'=>'required','message'=>'required','resume' => 'required|mimes:doc,docx,odt|max:2048');
	    $validator = Validator::make($request->all(), $rules); 
		$data = $request->all();
	
	    if ($validator->fails()) {
			return response()->json(array(
					'success' => false,
					'message' => $validator->getMessageBag()->toArray()
					));
	   }  
		// $timestmp=time();
		if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
		     return response()->json(array(
		            	'success' => false,
						'message' => "Invalid Email address."
						));
			}

		    $mob="/^[6789][0-9]{9}$/";
			if(!preg_match($mob,$data['mobile_number']))
			{	  return response()->json(array(
		            	'success' => false,
						'message' => "Invalid Mobile number."
						));
			}


	
		  // DB::table('count')->increment('presentcount');
		  // return response()->json(array(
		  //           	'success' => true,
				// 		'message' => "message is finished."
				// 		));



		


  

	
// die(json_encode($count[0]->presentcount));
		   // $usercont = DB::table('count')->where($count[0]->presentcount,$count[0]->presentcount)->count();

	

		
		// else{
			$name= $request->get('name');
                 $mobile_number= $request->get('mobile_number');
                        
                $email =$request->get('email');
                
               $address =$request->get('address');
				 $message =$request->get('message');
				 // echo $message;
		  $file = $request->file('resume');

       $allowed =  array('doc', 'docx','odt');
        $filename = $_FILES['resume']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) {
           return response()->json(array(
                                'success' => false,
                                'message' => "Extension error only  doc/docx  or odt  valid "
                        ));
                }
       
       
        if($file!=null) {   
        $file_cat="file"; 

        $i_year=date('Y'); $i_month=date('m'); 
        $file_name=uniqid();
         $ext=$file->getClientOriginalExtension(); 
        if (!is_dir($file_cat)) {
            	mkdir($file_cat);
        }
        
        if (!is_dir($file_cat . "/" . $i_year)) {
            mkdir($file_cat . "/" . $i_year);
        }
        if (!is_dir($file_cat . "/" . $i_year . "/" . $i_month)) {
            mkdir($file_cat . "/" . $i_year . "/" . $i_month);
        }
 

      

      $file->move($file_cat.'/'.$i_year.'/'.$i_month.'/', $file_name.'.'.$ext  );


  $path=public_path($file_cat.'/'.$i_year.'/'.$i_month.'/'.$file_name.'.'.$ext );
// echo $path;
 }
 
 
$subject="Job Application";
$email="noreplay@getnewsplus.com";
				Mail::send('email', [
				'name'=>$name,
				'email'=>$email,
				'phone'=>$mobile_number,
				'address'=>$address,
				'msg'=> $message,
				'file'=>$path,
				], function($m) use($email, $subject) {
				$m->from($email);
				$m->to('chipsyinfo@gmail.com')->subject($subject);
				});


				
		     //  $data['profile_id']=DB::getPdo()->lastInsertId();


		
 // $file=DB::insert('insert into file(profile_id,category,year,month,file_name,ext) values(?,?,?,?,?,?)',array($data['profile_id'],$file_cat,$i_year,$i_month,$file_name,$ext));

             







		   return response()->json(array(
		            	'success' => true,
		            	'message' => "Your profile Send Successfully "
		 				));
		 }
	
	}

         
		
  

 

	